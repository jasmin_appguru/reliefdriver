<?php

return array (
  'booking-menu' => 'Booking',
  'booking-page-title' => 'Booking Management',
  'booking-view-title' => 'Booking Detail',
  'dashboard-menu' => 'Statistics',
  'dashboard-page-title' => 'Statistics List',
  'driver-menu' => 'Driver',
  'driver-page-title' => 'Driver Management',
  'driver-view-title' => 'Driver Detail',
  'feedback-menu' => 'Feedback',
  'feedback-page-title' => 'Feedback Management',
  'feedback-view-title' => 'Feedback Detail',
  'notification-menu' => 'Notification',
  'notification-page-title' => 'Notification Management',
  'owner-menu' => 'Owner',
  'owner-page-title' => 'Owner Management',
  'owner-view-title' => 'Owner Detail',
);
