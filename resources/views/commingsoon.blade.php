<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

		<title>Relief Driver</title>

		<!-- ================ /GOOGLE FONT EOC =================== -->
		<link rel="icon" type="image/x-icon" href="assets/images/favicon.png">
		<link href="assets/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- ================ /GOOGLE FONT EOC =================== -->

		<!-- =================== /WEB CSS BOC ==================== -->
		<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap-5.0.2/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="./assets/font/roboto/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="./assets/css/animations.css">
		<link rel="stylesheet" type="text/css" href="./assets/css/slick.min.css">
		<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="./assets/css/media.css">

		<!-- =================== /WEB CSS EOC ==================== -->

		<style type="text/css">
			.show_hide {display:none; }
		</style>
	</head>
	<body id="home" class="homePage">

	<!-- ====== /WRAPPER BOC ====== -->
		<div class="page-wrapper ">
		  	<!-- header start -->
		  	<header class="site-header">
			    <div class="header-main nav-area">
			      	<div class="header-inner-main">
			      		<div class="container">
				          	<div class="navbar-container">

					            <nav class="navbar navbar-dark navbar-expand-lg">
					                <div class="header-logo">
					                	<a href="#"><img src="assets/images/Logo.png" alt="header-logo"></a>
					              	</div>
					              	<div class="header-inner d-flex justify-content-center">
						                <div class="header-right">
							                <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
							                    <span class="icon-bar"></span>
							                    <span class="icon-bar"></span>
							                    <span class="icon-bar"></span>
							                </button>

						                    <div class="collapse header-menu navbar-collapse justify-content-end" id="navbarNavDropdown">
						                      <div class="header-menu-inner">
						                        <ul class="navbar-nav" id="menu-center">
						                         <!--  <li class="nav-item active">
						                            <a href="#home" class="scrollTosection">Home</a>
						                          </li> -->
						                          <li class="nav-item">
						                            <a href="#Features" class="scrollTosection">App Features</a>
						                          </li>
						                          <li class="nav-item ">
						                            <a href="#About" class="scrollTosection">About Us	</a>
						                          </li>
						                          <li class="nav-item">
						                            <a href="#Contact" class="scrollTosection">Contact Us</a>
						                          </li>
                                                  <li class="nav-item">
                                                    <a href="#faq" class="scrollTosection">FAQ’s</a>
                                                  </li>
						                          <li class="nav-item">
						                            <a href="#download-app" class="scrollTosection">Download App</a>
						                          </li>
						                        </ul>
						                      </div>
						                    </div>
						                </div>
						            </div>
					            </nav>
				            </div>
			            </div>
			        </div>
			    </div>
		  	</header>
			<!-- header End -->

			<!-- Content Start -->


			<div class="page-loader">
				<div class="moderspinner"></div>
			</div>

			<section class="banner-sec" >
				<div class="banner-wrap">
					<div class="container">
						<div class="row flex-md-row-reverse align-items-center">

							<div class="col-lg-5 col-md-6 col-sm-12 col-12 animatedParent animateOnce">
								<div class="banner-img-wp">
									<div class="banner-img animated fadeInRight slow">
										<img src="assets/images/banner-mockup.png" alt="#">
									</div>
								</div>
							</div>
							<div class="col-lg-7 col-md-6 col-sm-12 col-12 animatedParent animateOnce" >
								<div class="banner-text animated fadeInUpShort" id="app-link-main">
									<h2 class="">Find & book a <br><span class="light-green-text">Relief Driver</span> on demand.</h2>
									<p>A tailored quick and easy mobile app that allows Owner Drivers to find their preferred Relief Driver on demand. Plan in advance. <a href="#" class="booknow-link"> Book Now!</a></p>
									<div class="banner-btn">
										<ul>
											<li><a href="https://apps.apple.com/au/app/relief-driver/id1615576606" class="animated bounceIn slow"><img src="assets/images/app-strore.png" alt="#"></a></li>
											<li><a href="https://play.google.com/store/apps/details?id=com.reliefdriver" class="animated bounceIn slow"><img src="assets/images/google-pay.png" alt="#"></a></li>
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>

			<section class="features-sec" id="Features">
				<div class="features-inner">
					<div class="container">
						<div class="features-wrap">
							<div class="section-title text-center animatedParent animateOnce">
								<h2 class="animated fadeInUpShort"><span class="light-green-text">App </span> Features</h2>
								<p class="animated fadeInUpShort">Relief Driver has been calibrated with key features that are <br/>essential, quick and easy on the go </p>
							</div>
							<div class="row animatedParent animateOnce" data-sequence="400">
								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="1">
										<h3>Search</h3>
										<p>Input your required date(s) to find available Relief Drivers. You can refine your search criteria with Filters based on experience, rate and company inducted into. </p>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="2">
										<h3>Bookings</h3>
										<p>Accept / Decline booking requests sent by Owner Drivers. Be quick to not miss out on booking opportunities. You can cancel a booking any time.</p>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="3">
										<h3>Notifications</h3>
										<p>You will receive notifications in your notification centre. Notifications will be sent for booking requests, acceptance of booking requests, 24 hours and 3 hours before booking start time and cancellations.</p>
									</div>
								</div>

								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="4">
										<h3>Availability</h3>
										<p>Select the days of the week you are available and mark off Any future dates on the calendar you are unavailable. You will not appear in the search results for these dates.</p>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="5">
										<h3>Rates</h3>
										<p>Add your day and night rate. Owner Drivers will send you booking requests based on the rate they see on your profile. </p>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="6">
										<h3>License Class</h3>
										<p>Manage your license classes as per the licenses you hold to receive compatible booking requests. </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="about-sec" id="About">
				<div class="about-wrap">
					<div class="container">
						<div class="about-main">
							<div class="row flex-md-row-reverse align-items-center">
								<div class="col-lg-7 col-md-7 col-sm-12 col-12">
									<div class="about-text-wrap ">
										<div class="section-title animatedParent animateOnce">
											<h2 class="animated fadeInUpShort">About <span class="light-green-text">Relief Driver </span> </h2>
										</div>

										<div class="about-text-inner animatedParent animateOnce " data-sequence="400">
											<div class="about-text-box animated fadeInRightShort" data-id="1">
												<p>We created Relief Driver to eliminate the booking hassles most Owner Drivers and Relief Drivers experience. Based on market research and identified issues in the industry we have tailored a one stop mobile application that will service the booking needs of Owner Drivers and Relief Drivers. </p>
											</div>

											<div class="about-text-box vision-mission animated fadeInRightShort" data-id="2">
												<h3>Vision & Mission</h3>
												<p>To provide an essential relief driver booking service within the private and commercial transportation industry.  </p>
											</div>


										</div>
									</div>
								</div>
								<div class="col-lg-5 col-md-5 col-sm-12 col-12">
									<div class="about-img-wrap animatedParent animateOnce">
										<div class="about-img animated fadeInLeft slower">
											<img src="assets/images/rd-about-mockup.png" alt="#">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="contact-us-sec" id="Contact">
				<div class="contact-us-inner">
					<div class="container">
						<div class="contact-us-wrap">
							<div class="contactUs-content">

								<div class="section-title animatedParent animateOnce">
									<div class="animated fadeInUpShort">
										<h2>Contact Us</h2>
									</div>
								</div>

								<div class="contact-link-main">
									<div class="contact-wrap">
										<div class="content-left">
											<p>For all general enquiries / business partnerships</p>
										</div>
										<div class="content-right">
											<h5>General</h5>
											<a href="mailto:info@reliefdriver.co" class="contact-link">info@reliefdriver.co</a>
										</div>
									</div>
									<div class="contact-wrap">
										<div class="content-left">
											<p>If you experience any form of technical difficulties, <br/>please report to us immediately.</p>
										</div>
										<div class="content-right">
											<h5>Tech Support</h5>
											<a href="mailto:support@reliefdriver.co" class="contact-link">support@reliefdriver.co</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

      <section class="faqs-section" id="faq">
				<div class="faqs-inner">
					<div class="container">
						<div class="faqs-wrap">
							<div class="section-title animatedParent animateOnce">
								<div class="animated fadeInUpShort text-center">
									<h2>Frequently Asked <span class="light-green-text">Questions</span></h2>
								</div>
							</div>
							<div class="faqs-wp">
								<div class="accordions faqs-accordions" id="accordionExample">
									<div class="accordion-item">
									  <h2 class="accordion-header" id="headingOne">
										<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
										  <h6 class="mb-0">Is there a booking fee?</h6>
										</button>
									  </h2>
									  <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
											<div class="accordion-body">
												<p>There are no associated fees to book a Relief Driver. You simply subscribe for $9.99 per week to use the app.</p>
											</div>
									  </div>
									</div>
									<div class="accordion-item">
									  <h2 class="accordion-header" id="headingTwo">
										<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										  <h6>Can I pay my Relief Driver through the app? </h6>
										</button>
									  </h2>
									  <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
											<div class="accordion-body">
												<p>For now, we are only facilitating bookings. Please pay your Relief Driver via their preferred payment method.</p>
											</div>
									  </div>
									</div>
									<div class="accordion-item">
									  <h2 class="accordion-header" id="headingThree">
										<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										  <h6>What happens if my Relief Driver doesn't show up? </h6>
										</button>
									  </h2>
									  <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
										<div class="accordion-body">
											<p>A Relief Driver who doesn’t show or notify you of changes of circumstances and availability to do the booked job is classified unreliable. We recommend that you book a different Relief Driver in the future.</p>
										</div>
									  </div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="headingFour">
										  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
											<h6>What happens if I'm not available on all 7 days of the week?</h6>
										  </button>
										</h2>
										<div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
											<div class="accordion-body">
												<p>You will miss out on booking requests for bigger jobs. If an Owner is searching to book a Driver for a full 2 weeks for example, and you have not ticked 1 particular day of the week on your profile, you will not appear in the search results. Therefore, to increase your visibility, tick all days of the week so you don't miss out on booking requests. It is still up to you to decide if you would like to accept the booking.</p>
											</div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="headingFive">
										  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
											<h6>Can I sign up as an Owner and Relief Driver with one account? </h6>
										  </button>
										</h2>
										<div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
											<div class="accordion-body">
												<p>No. You need to have a separate account as an Owner Driver and Relief Driver however you can use the same email address for both.</p>
											</div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="headingSix">
										  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
											<h6>Can I use the same mobile number for my Owner and Relief Driver account?</h6>
										  </button>
										</h2>
										<div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample">
											<div class="accordion-body">
												<p>Yes. You can only use the same mobile number once for an Owner account and Relief Driver account. If you try to sign up as an Owner again with a new account, when you already have an Owner account with the same number, it will not allow you. You will receive this popup message “You are already registered with this mobile number.” </p>
											</div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="headingSeven">
										  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
											<h6>What do I do if there are any issues with the app? </h6>
										  </button>
										</h2>
										<div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
											<div class="accordion-body">
												<p>If you encounter any issue, please report it to us via the Feedback section in the app or email support@reliefdriver.co. Our aim is to keep the app operating at an optimal level however due to unforeseen changes in operating systems and devices, unexpected issues may occur. It is highly recommended that you ensure your mobile device has the latest operating system update installed at all times.</p>
											</div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="headingEight">
										  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
											<h6>How do I subscribe or restore my subscription?  </h6>
										  </button>
										</h2>
										<div id="collapseEight" class="accordion-collapse collapse" aria-labelledby="headingEight" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                <p><b>Sign in for the first time</b></p>
                                                <p>After sign up, you need to subscribe first and then complete your profile to use the app </p>
                                                <br>
                                                <p><b>Sign in on a new device with an existing subscription</b></p>
                                                <p>If you are using the same Apple or Google ID, you need to click RESTORE in the top right corner of the subscription screen to restore it on your new device. If you are using a new Apple or Google ID, you will need to subscribe again. Subscriptions are linked to your Apple or Google ID. If you try to RESTORE your subscription when you don’t have one, this message will show “No active subscription.” </p>
                                            </div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="headingNine">
										  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
											<h6>I forgot my password and can’t get into my account.</h6>
										  </button>
										</h2>
										<div id="collapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine" data-bs-parent="#accordionExample">
											<div class="accordion-body">
												<p>You will need to reset your password. </p>
												<br>
												<p>1. Click on Forgot Password? from the Sign In screen</p>
												<p>2. Input the email address you signed up with</p>
												<p>3. You will receive an email with a link to reset your password. There is a 15 min expiry on this link therefore if you do not reset on time, you will need to go through the process again.</p>
												<p>4. Once you have reset your password, go back to the app and Sign In. </p>
												<p>5. If you experience any issues with the reset password link, do a hard refresh in your browser and try again.<br>
												<b>Mac = Command + R / Windows = Control + R</b> </p>
												<p class="ms-4">○	If you are on a mobile device, clear your web browser cache and try again. </p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>



			<section>
				<div class="app-screenshot-sec" id="app-screenshot">
					<div class="container">
						<div class="app-screenshot-wrap">
							<div class="app-screenshot-slider">
								<div class="screenshot">
									<img src="assets/images/app-screenshot/app-screenshot-1.png" alt="#">
								</div>
								<div class="screenshot">
									<img src="assets/images/app-screenshot/app-screenshot-2.png" alt="#">
								</div>
								<div class="screenshot">
									<img src="assets/images/app-screenshot/app-screenshot-3.png" alt="#">
								</div>
								<div class="screenshot">
									<img src="assets/images/app-screenshot/app-screenshot-4.png" alt="#">
								</div>
								<div class="screenshot">
									<img src="assets/images/app-screenshot/app-screenshot-5.png" alt="#">
								</div>
								<div class="screenshot">
									<img src="assets/images/app-screenshot/app-screenshot-6.png" alt="#">
								</div>
								<div class="screenshot">
									<img src="assets/images/app-screenshot/app-screenshot-7.png" alt="#">
								</div>
								<div class="screenshot">
									<img src="assets/images/app-screenshot/app-screenshot-8.png" alt="#">
								</div>
								<div class="screenshot">
									<img src="assets/images/app-screenshot/app-screenshot-9.png" alt="#">
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section>
				<div class="download-app-sec" id="download-app">
					<div class="container">
						<div class="download-app-wrap animatedParent animateOnce">
							<div class="row align-items-center">
								<div class="col-lg-7 col-md-6 col-sm-12 col-12">
									<div class="section-title animated fadeInUpShort">
										<h2><span class="light-green-text">Download</span> the app</h2>
									</div>
								</div>
								<div class="col-lg-5 col-md-6 col-sm-12 col-12">
									<div class="download-app-links">
										<div class="banner-btn">
											<ul>
												<li><a href="https://apps.apple.com/au/app/relief-driver/id1615576606"	 class="animated bounceIn slow"><img src="assets/images/app-strore.png" alt="#"></a></li>
												<li><a href="https://play.google.com/store/apps/details?id=com.reliefdriver"	 class="animated bounceIn slow"><img src="assets/images/google-pay.png" alt="#"></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</section>

			<!-- Content End -->
		</div>

		<!-- ====== FOOTER BOC ====== -->
		<footer>
		    <div class="footer-main">
		    	<div class="container">
		    		<div class="footer-text">
		    			<div class="footer-left">
		    				<p>© {{ date('Y') }} Relief Driver. All Rights Reserved.</p>
		    			</div>
		    			<div class="footer-right">
		    				<h5>Developed by</h5>
		    				<div class="developed-by-img"><a href="https://www.appgurus.com.au/" target="_blank"><img src="assets/images/appguruslogo.png" alt="appgurus"></a></div>
		    			</div>
		    		</div>

		    	</div>
		    </div>
		</footer>
		<!-- ====== FOOTER End ====== -->

		<style type="text/css">
			.footer-main .footer-text .footer-left p{text-align:left;}
			.footer-main .footer-text{display:flex;justify-content:space-between;align-items:center;flex-wrap:wrap;}
			.footer-main .footer-text .footer-right{display:flex;}
			.footer-main .footer-text .footer-right .developed-by-img{width:191px;height:38px;}
			.footer-main .footer-text .footer-right h5{color:#fff;margin-bottom:0px;font-size:24px;font-weight:400;line-height:38px;height:38px;padding-top:6px;padding-right:10px;}

			@media (max-width:1199px){
				.footer-main .footer-text .footer-right h5{font-size:20px;line-height:33px;height:33px;padding-top:4px;}
				.footer-main .footer-text .footer-right .developed-by-img{width:155px;height:33px;}
			}

			@media (max-width:991px){
				.footer-main .footer-text{padding:15px 0px;}
			}

			@media (max-width:767px){
				.footer-main .footer-text .footer-left{width:100%;}
				.footer-main .footer-text .footer-right{width:100%;justify-content:center;padding-top:10px;padding-bottom:5px;}
				.footer-main .footer-text .footer-left p{text-align:center;}
				.footer-main .footer-text .footer-right h5{font-size:18px;line-height:30px;height:30px;padding-top:3px;}
				.footer-main .footer-text .footer-right .developed-by-img{width:141px;height:30px;}
			}
		</style>



	</body>
	<script type="text/javascript" src="./assets/js/jquery-3.6.0/jquery.min.js"></script>
	<script type="text/javascript" src="./assets/js/bootstrap-5.0.2/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="./assets/js/css3-animate-it.js"></script>
	<script type="text/javascript" src="./assets/js/slick.min.js"></script>
	<script type="text/javascript" src="./assets/js/custom.js"></script>



</html>
