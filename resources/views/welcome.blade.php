
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

		<title>Home Page</title>

		<!-- ================ /GOOGLE FONT EOC =================== -->
		<link rel="icon" type="image/x-icon" href="{{ asset('front_assets/assets/images/favicon.png') }}">
		<link href="{{ ('front_assets/font/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
		<!-- ================ /GOOGLE FONT EOC =================== -->

		<!-- =================== /WEB CSS BOC ==================== -->
		<link rel="stylesheet" type="text/css" href="{{ asset('front_assets/css/bootstrap-5.0.2/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('front_assets/font/roboto/stylesheet.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('front_assets/css/animations.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('front_assets/css/style.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('front_assets/css/media.css') }}">

		<!-- =================== /WEB CSS EOC ==================== -->

		<style type="text/css">
			.show_hide {display:none; }
		</style>
	</head>
	<body id="home">

	<!-- ====== /WRAPPER BOC ====== -->
		<div class="page-wrapper ">
		  	<!-- header start -->
		  	<header class="site-header">
			    <div class="header-main nav-area">
			      	<div class="header-inner-main">
			      		<div class="container">
				          	<div class="navbar-container">

					            <nav class="navbar navbar-dark navbar-expand-lg">
					                <div class="header-logo">
					                	<a href="./listed.html"><img src="front_assets/images/logo.png" alt="header-logo"></a>
					              	</div>
					              	<div class="header-inner d-flex justify-content-center">
						                <div class="header-right">
							                <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
							                    <span class="icon-bar"></span>
							                    <span class="icon-bar"></span>
							                    <span class="icon-bar"></span>
							                </button>

						                    <div class="collapse header-menu navbar-collapse justify-content-end" id="navbarNavDropdown">
						                      <div class="header-menu-inner">
						                        <ul class="navbar-nav" id="menu-center">
						                          <li class="nav-item active">
						                            <a href="#home" class="scrollTosection">Home</a>
						                          </li>
						                          <li class="nav-item">
						                            <a href="#Features" class="scrollTosection">App Features</a>
						                          </li>
						                          <li class="nav-item ">
						                            <a href="#About" class="scrollTosection">About us	</a>
						                          </li>
						                          <li class="nav-item">
						                            <a href="#Contact" class="scrollTosection">Contact us</a>
						                          </li>
						                        </ul>
						                      </div>


						                    </div>
						                </div>
						                <div class="login-btn">
						                	<ul>
						                		<li><a href="javascript:void(0)">Login</a></li>
						                		<li><a href="javascript:void(0)" class="btn">Register</a></li>
						                	</ul>
						                </div>
						            </div>
					            </nav>
				            </div>
			            </div>
			        </div>
			    </div>
		  	</header>
			<!-- header End -->

			<!-- Content Start -->


			<div class="page-loader">
				<div class="moderspinner"></div>
			</div>

			<section class="banner-sec" >
				<div class="banner-wrap">
					<div class="container">
						<div class="row flex-md-row-reverse">

							<div class="col-lg-6 col-md-12 col-sm-12 col-12 animatedParent animateOnce">
								<div class="banner-img-wp">
									<div class="banner-img animated fadeInRight">
										<img src="front_assets/images/banner-img.png" alt="#">
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 col-12 animatedParent animateOnce" >
								<div class="banner-text animated fadeInUpShort" id="app-link-main">
									<h2 class="">Lorem ipsum dolor <br>sit amet, <span class="pink-text">consetetur</span><br> sadipscing elitr</h2>
									<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores</p>
									<div class="banner-btn">
										<ul>
											<li><a href="#" class="animated bounceIn slow"><img src="front_assets/images/app-strore.png" alt="#"></a></li>
											<li><a href="#" class="animated bounceIn slow"><img src="front_assets/images/google-pay.png" alt="#"></a></li>
										</ul>
									</div>

									<div class="subscribe-form-main">
										<div class="subscribe-wp">
											<input type="email" class="form-control" placeholder="Enter your mail…">
											<button class="btn subscribe-btn">Subscribe</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>

			<section class="features-sec" id="Features">
				<div class="features-inner">
					<div class="container">
						<div class="features-wrap">
							<div class="section-title text-center animatedParent animateOnce">
								<h2 class="animated fadeInUpShort"><span class="pink-text">Relief Driver </span> Features</h2>
								<p class="animated fadeInUpShort">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam <br> nonumy eirmod tempor invidunt ut labore </p>
							</div>
							<div class="row animatedParent animateOnce" data-sequence="400">
								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="1">
										<div class="features-icon">
											<i class="fa fa-cogs" aria-hidden="true"></i>
										</div>
										<h3>Lorem ipsum</h3>
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore </p>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="2">
										<div class="features-icon">
											<i class="fa fa-cogs" aria-hidden="true"></i>
										</div>
										<h3>Lorem ipsum</h3>
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore </p>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="3">
										<div class="features-icon">
											<i class="fa fa-cogs" aria-hidden="true"></i>
										</div>
										<h3>Lorem ipsum</h3>
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore </p>
									</div>
								</div>

								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="4">
										<div class="features-icon">
											<i class="fa fa-cogs" aria-hidden="true"></i>
										</div>
										<h3>Lorem ipsum</h3>
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore </p>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="5">
										<div class="features-icon">
											<i class="fa fa-cogs" aria-hidden="true"></i>
										</div>
										<h3>Lorem ipsum</h3>
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore </p>
									</div>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-12">
									<div class="features-box animated fadeInUpShort" data-id="6">
										<div class="features-icon">
											<i class="fa fa-cogs" aria-hidden="true"></i>
										</div>
										<h3>Lorem ipsum</h3>
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="about-sec" id="About">
				<div class="about-wrap">
					<div class="container">
						<div class="row flex-md-row-reverse">
							<div class="col-lg-5 col-md-12 col-sm-12 col-12">
								<div class="about-text-wrap ">
									<div class="section-title animatedParent animateOnce">
										<h2 class="animated fadeInUpShort">About <span class="pink-text">Relief Driver </span> </h2>
									</div>

									<div class="about-text-inner animatedParent animateOnce " data-sequence="400">
										<div class="about-text-box animated fadeInRightShort" data-id="1">
											<h3>Lorem ipsum</h3>
											<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna  aliquyam erat, sed diam voluptua. </p>
										</div>
										<div class="about-text-box animated fadeInRightShort" data-id="2">
											<h3>Lorem ipsum</h3>
											<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna  aliquyam erat, sed diam voluptua. </p>
										</div>
										<div class="about-text-box animated fadeInRightShort" data-id="3">
											<h3>Lorem ipsum</h3>
											<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna  aliquyam erat, sed diam voluptua. </p>
										</div>
										<div class="about-text-box animated fadeInRightShort" data-id="4">
											<div class="about-btn">
												<a href="#" class="btn">Contact Us</a>
											</div>
										</div>

									</div>
								</div>
							</div>
							<div class="col-lg-7 col-md-12 col-sm-12 col-12">
								<div class="about-img-wrap animatedParent animateOnce">
									<div class="about-img animated fadeInLeft slower">
										<img src="front_assets/images/about-img.png" alt="#">
									</div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</section>

			<section class="contact-us-sec" id="Contact">
				<div class="contact-us-inner">
					<div class="container">
						<div class="contact-us-wrap">
							<div class="contactUs-content">

								<div class="section-title animatedParent animateOnce">
									<div class="animated fadeInUpShort">
										<h2>Contact Us</h2>
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam <br> Nonumy eirmod tempor invidunt ut labore </p>
									</div>
								</div>

								<div class="form-main">
									<form>
										<div class="row animatedParent animateOnce" data-sequence="400">
											<div class="col-lg-6 col-md-6 col-sm-6 col-12">
												<div class="form-group animated fadeInUpShort" data-id="1">
													<input type="text"  class="form-control" placeholder="First name">
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-12">
												<div class="form-group animated fadeInUpShort" data-id="2">
													<input type="text"  class="form-control" placeholder="Last Name">
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-12">
												<div class="form-group animated fadeInUpShort" data-id="3">
													<input type="text"  class="form-control" placeholder="Mobile No">
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-12">
												<div class="form-group animated fadeInUpShort" data-id="4">
													<input type="text"  class="form-control" placeholder="Email ID">
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-12">
												<div class="form-group animated fadeInUpShort" data-id="5">
													<button class="btn submit-btn">Submit Now</button>
												</div>
											</div>
										</div>
									</form>
								</div>

								<div class="contact-us-bottom">
									<div class="contact-b-row animatedParent animateOnce" data-sequence="400">
										<div class="contact-b-col animated fadeInUpShort" data-id="1">
											<h5>Email Us</h5>
											<a href="mailto:test@Relief Driver.com" class="contact-link">test@Relief Driver.com</a>
										</div>
										<div class="contact-b-col animated fadeInUpShort" data-id="2">
											<h5>Phone No</h5>
											<a href="tell:+61 - 1234 567 890" class="contact-link">+61 - 1234 567 890</a>
										</div>
										<div class="contact-b-col animated fadeInUpShort" data-id="3">
											<h5>Social Contact</h5>
											<ul class="social-link">
												<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>


			<section>
				<div class="subscribe-sec animatedParent animateOnce">
					<div class="container">
						<div class="section-title text-center animated fadeInUpShort">
							<h2>Subscribe Newsletter & Get Latest <br>Update about <span class="pink-text">Relief Driver</span></h2>
						</div>
						<div class="subscribe-form-main">
							<div class="subscribe-wp animated bounceInUp slower">
								<input type="email" class="form-control" placeholder="Enter your mail…">
								<button class="btn subscribe-btn">Subscribe</button>
							</div>
						</div>
					</div>
				</div>
			</section>


			<section>
				<div class="app-screenshot-sec">
					<div class="app-screenshot-img">
						<img src="front_assets/images/app-screenshot.png" alt="#">
						<a href="#app-link-main" class="link-app"></a>
					</div>
				</div>
			</section>

			<!-- Content End -->
		</div>

		<!-- ====== FOOTER BOC ====== -->
		<footer>
		    <div class="footer-main">
		    	<div class="container">
		    		<div class="footer-text">
		    			<p>© 2022 Relief Driver. All Rights Reserved.</p>
		    		</div>
		    	</div>
		    </div>
		</footer>
		<!-- ====== FOOTER End ====== -->




	</body>
	<script type="text/javascript" src="{{ asset('front_assets/js/jquery-3.6.0/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front_assets/js/bootstrap-5.0.2/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front_assets/js/css3-animate-it.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front_assets/js/custom.js') }}"></script>



</html>
