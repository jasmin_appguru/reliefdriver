@extends('admin.layouts.main')
@section('content')
  
  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">Affiliation Management</h1>
      <ol class="breadcrumb">
        {{-- <li class="breadcrumb-item"><a href="../index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li>
        <li class="breadcrumb-item active">DataTables</li> --}}
      </ol>
    </div>

    <div class="page-content">
      <!-- Panel Basic -->
      <div class="panel">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h3 class="panel-title">  </h3>
        </header>
        <div class="panel-body app-contacts">
          <table class="table table-hover dataTable table-striped w-full" id="data-table">
            <thead>
              <tr>
                <th>ID</th>
                <th>Image</th>
                <th>Link</th>
              </tr>
            </thead>
            {{-- <tfoot>
              <tr>
                <th>ID</th>
                <th>Image</th>
                <th>Link</th>
              </tr>
            </tfoot> --}}
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
      <!-- End Panel Basic -->

      <div class="site-action" data-plugin="actionBtn">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus animation-scale-up" onclick="openClinetModal()" aria-hidden="true"></i>
      <i class="back-icon md-close animation-scale-up" aria-hidden="true"></i>
    </button>
    <div class="site-action-buttons">
      <button type="button" data-action="trash" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon md-delete" aria-hidden="true"></i>
      </button>
      <button type="button" data-action="folder" class="btn-raised btn btn-success btn-floating animation-slide-bottom">
        <i class="icon md-folder" aria-hidden="true"></i>
      </button>
    </div>
  </div>

  <!-- Add Client Form -->
  <div class="modal fade" id="addUserForm" aria-hidden="true" aria-labelledby="addUserForm"
    role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Add Affiliate</h4>
        </div>
        <div class="modal-body">
          <form id="addAff" action="{{ route('createAffiliate') }}" method="post" enctype="multipart/form-data" >
            @csrf


            <div class="form-group"><input type="file" id="affimage" name="affimage"  class="form-control" placeholder="Image" style="padding: 3px;"></div>
            <div class="form-group"><input type="text" name="afflink" id="afflink" class="form-control" placeholder="Link"/></div>

        
          <div class="modal-footer">
            <button type="submit" class="btn btn-add" style="background-color: #6C863D;color: white;">Save</button>
            <button type="button" class="btn btn-cancel" data-dismiss="modal" style="background-color: white; color: black;">Cancel</button>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <!-- End Add Client Form -->

      


    </div>
  </div>
  <!-- End Page -->


  
@stop

@section('footer_script')

<script>

   function openClinetModal()
    {
      $('#addUserForm').modal('show');
    }

    $(document).ready(function(){
      // alert("hedrdsf");
        var dt = $('#data-table').DataTable({
            "aaSorting": [],
            "stateSave": false,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            //"iDisplayLength": 5,
            //"lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
            processing: true,
            serverSide: true,
            ajax: '{{url("admin/Aff/getAllData")}}',
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'}
            ]
        });
    });


</script>

@stop