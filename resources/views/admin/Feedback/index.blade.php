@extends('admin.layouts.main')
@section('content')

<style>
    table.display {
  margin: 0 auto;
  width: 100%;
  clear: both;
  border-collapse: collapse;
  table-layout: fixed;         // add this
  word-wrap:break-word;        // add this
}
</style>

  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">{{ __('admin.feedback-page-title') }}</h1>
      <ol class="breadcrumb">
        {{-- <li class="breadcrumb-item"><a href="../index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li>
        <li class="breadcrumb-item active">DataTables</li> --}}
      </ol>
    </div>

    <div class="page-content">
      <!-- Panel Basic -->
      <div class="panel">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h3 class="panel-title">  </h3>
        </header>
        <div class="panel-body app-contacts">
          {{-- <div class="panel-heading">
              <div class="text-right">
                  <a href="{{ url('/admin/exportAgentData') }}" class="btn btn-flat btn-success">Download CSV</a>
              </div>
          </div> --}}
          <div class="row">
            <div class="col-md-12 text-right">
              <div class="mb-15">

              </div>
            </div>
          </div>
          <div class="table-responsive">
          <table class="table table-hover dataTable table-striped w-full display" id="data-table">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Feedback</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          </div>
        </div>
      </div>
      <!-- End Panel Basic -->
    </div>
  </div>
  <!-- End Page -->
@stop

@section('footer_script')
<script>
    $(document).ready(function(){
      // alert("hedrdsf");
        var dt = $('#data-table').DataTable({
            "aaSorting": [],
            "stateSave": false,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "iDisplayLength": 100,
            "lengthMenu": [[10,100, 150, 200, -1], [10,100, 150, 200, "All"]],
            "bAutoWidth": false,
            processing: true,
            serverSide: true,
            ajax: '{{url("admin-api/feedback-list/getAllData")}}',
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false,width: "10px"},
                {data: 'name', name: 'name', width: "100px" , sWidth: '100px' },
                {data: 'email', name: 'email' , width: "150px" },
                {data: 'mobile', name: 'mobile' , width: "150px"},
                {data: 'feedback_text', name: 'feedback_text'},
            ]
        });
    });


</script>

@stop
