@extends('admin.layouts.main')
@section('content')


<div class="page">
   <div class="page-header">
      <h1 class="page-title">{{ __('admin.dashboard-page-title') }}</h1>
    </div>
    <div class="page-content container-fluid">
        <div class="container-fluid p-3"  >
            <form action="{{ route('filter') }}" method="POST">
            <div class="row" >
                @csrf

              <div class="col-sm-3">
                <div class="form-group">
                  <select name="range" id="globalRange" class="form-control" required>
                      <option value="all">All</option>
                      <option value="daily" {{ $range == "daily" ? "selected" : "" }} >Daily</option>
                      <option value="weekly"  {{ $range == "weekly" ? "selected" : "" }}>Weekly</option>
                      <option value="monthly"  {{ $range == "monthly" ? "selected" : "" }}>Monthly</option>
                      <option value="quarterly"  {{ $range == "quarterly" ? "selected" : "" }}>Quarterly</option>
                      <option value="yearly"  {{ $range == "yearly" ? "selected" : "" }}>Yearly</option>
                      <option value="custom"  {{ $range == "custom" ? "selected" : "" }}>Custom Date A - B </option>
                      <option value="todate"  {{ $range == "todate" ? "selected" : "" }}>To Date</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-9">
                <div class="form-group">
                    <button class="btn btn-primary" id="applyFilter" ><i class="fa fa-filter fa-fw" ></i>Filter</button>
                </div>
              </div>

              <div class="col-sm-3 custome" style="display: {{ $range == "custom" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">Start Date</label>
                  <input type="date" name="start_date" max="{{ date('Y-m-d') }}" class="form-control" value="{{ $start_date ? $start_date : "" }}">
                </div>
              </div>

              <div class="col-sm-3 custome" style="display: {{ $range == "custom" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">End Date</label>
                  <input type="date" name="end_date"  max="{{ date('Y-m-d') }}" class="form-control" value="{{ $end_date ? $end_date : "" }}">
                </div>
              </div>

              <div class="col-sm-3 todate" style="display: {{ $range == "todate" ? "" : "none" }};">
                <div class="form-group">
                  <label for="">End Date</label>
                  <input type="date" name="to_date"   max="{{ date('Y-m-d') }}"class="form-control" value="{{ $to_date ?? "" }}">
                </div>
              </div>

              <div class="col-sm-6 todate" style="display: none;">

              </div>

              {{-- <div class="col-sm-4" >
                <div class="form-group" >
                  <button class="btn btn-info" id="applyFilter" ><i class="fa fa-filter fa-fw" ></i>Filter</button>
                </div>
              </div> --}}

            </div>
        </form>
            <!-- Small boxes (Stat box) -->
        </div>
      	<div class="row">
			<div class="col-xl-12 col-lg-12">
	          <div class="row">
                <div class="col-lg-3">
                    <div class="card card-block p-25">
                      <div class="counter counter-lg">
                        <span class="counter-number">{{ $Owner }}</span>
                        <div class="counter-label text-uppercase">Owners</div>
                      </div>
                    </div>
                </div>
	            <div class="col-lg-3">
	              <div class="card card-block p-25">
	                <div class="counter counter-lg">
	                  <span class="counter-number">{{ $Driver }}</span>
	                  <div class="counter-label text-uppercase">Drivers</div>
	                </div>
	              </div>
	            </div>


	            <div class="col-lg-3">
	              <div class="card card-block p-25">
	                <div class="counter counter-lg">
	                  <span class="counter-number">{{ $feedback }}</span>
	                  <div class="counter-label text-uppercase">Feedback</div>
	                </div>
	              </div>
	            </div>
                <div class="col-lg-3">
                </div>
                <div class="col-lg-3">
                    <div class="card card-block p-25">
                      <div class="counter counter-lg">
                        <span class="counter-number">{{ $NextBooking }}</span>
                        <div class="counter-label text-uppercase">Completed BOOKINGS Days</div>
                      </div>
                    </div>
                  </div>
                <div class="col-lg-3">
                    <div class="card card-block p-25">
                      <div class="counter counter-lg">
                        <span class="counter-number">{{ $booking }}</span>
                        <div class="counter-label text-uppercase">UPCOMING BOOKING Days</div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3"></div>
                  <div class="col-lg-3"></div>
            <div class="col-lg-3">
                <div class="card card-block p-25">
                <div class="counter counter-lg">
                    <span class="counter-number">$ {{ $bookingAmount }}</span>
                    <div class="counter-label text-uppercase">COMPLETED BOOKING AMOUNT</div>
                </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card card-block p-25">
                <div class="counter counter-lg">
                    <span class="counter-number">$ {{ $bookingCurrent }}</span>
                    <div class="counter-label text-uppercase">UPCOMING BOOKING AMOUNT</div>
                </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card card-block p-25">
                <div class="counter counter-lg">
                    <span class="counter-number">$ {{ $bookingCNAmount }}</span>
                    <div class="counter-label text-uppercase">Cancellation Amount</div>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>

</div>
</div>
@stop

@section('footer_script')
<script>
    $(document).ready(function(){
        // alert("load");
        $("#globalRange").change(function(){
            if($(this).val()=="custom")
            {
                $(".custome").show();
            }
            else
            {
                $(".custome").hide();
            }
            if($(this).val()=="todate")
            {
                $(".todate").show();
            }
            else
            {
                $(".todate").hide();
            }

        });
    });
</script>
@stop
