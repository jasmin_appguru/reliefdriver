<div class="site-menubar site-menubar-light">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu" data-plugin="menu">
            <li class="site-menu-item @if(in_array('dashboard', Request::segments())) active @endif">
              <a href="{{route('admin.dashboard')}}">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.dashboard-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('owner-user', Request::segments())) active @endif">
              <a href="{{url('admin/owner-user')}}">
                <i class="site-menu-icon md-accounts-list" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.owner-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('driver-user', Request::segments())) active @endif">
              <a href="{{url('admin/driver-user')}}">
                <i class="site-menu-icon md-accounts-list" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.driver-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('Booking', Request::segments())) active @endif">
              <a href="{{url('admin/Booking')}}">
                <i class="site-menu-icon md-comment-list" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.booking-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('Feedback', Request::segments())) active @endif">
              <a href="{{url('admin/Feedback')}}">
                <i class="site-menu-icon md-comment-list" aria-hidden="true"></i>
                <span class="site-menu-title">{{ __('admin.feedback-menu') }}</span>
              </a>
            </li>
            <li class="site-menu-item @if(in_array('Notification', Request::segments())) active @endif">
              <a href="{{url('admin/Notification')}}">
                <i class="site-menu-icon icon md-notifications"></i>
                <span class="site-menu-title">{{ __('admin.notification-menu') }}</span>
              </a>
            </li>
            {{-- <li class="site-menu-item @if(in_array('languages', Request::segments())) active @endif">
              <a href="{{url('languages/en/translations?filter=&language=en&group=api')}}">
                <i class="site-menu-icon md-text-format"></i>
                <span class="site-menu-title">Alert Massages</span>
              </a>
            </li> --}}
            <!-- <li class="site-menu-item @if(in_array('promocode-management', Request::segments())) active @endif">
              <a href="{{url('admin/promocode-management')}}">
                <i class="site-menu-icon md-accounts-list" aria-hidden="true"></i>
                <span class="site-menu-title">Promocode</span>
              </a>
            </li> -->
            <!--
            <li class="site-menu-item has-sub @if(in_array('notification-messages', Request::segments())) active @endif">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-comment-list" aria-hidden="true"></i>
                <span class="site-menu-title">Messages</span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a href="javascript:void(0)">
                    <span class="site-menu-title">Push Notification</span>
                  </a>
                </li>
                <li class="site-menu-item @if(in_array('notification-messages', Request::segments())) active @endif">
                  <a href="{{url('admin/notification-messages')}}">
                    <span class="site-menu-title">Notification Messages</span>
                  </a>
                </li>
              </ul> -->

          </ul>
        </div>
      </div>
    </div>
  </div>
