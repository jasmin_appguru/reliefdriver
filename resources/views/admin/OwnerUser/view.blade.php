<div class="modal-dialog modal-dialog-slideout" role="document">
    <div class="modal-content">
      <div class="modal-header slidePanel-header bg-light-green-600">
        <div class="overlay-top overlay-panel overlay-background bg-light-green-600">
          <div class="slidePanel-actions btn-group btn-group-flat" aria-label="actions" role="group">
            <!-- <button type="button" class="btn btn-pure icon md-edit subtask-toggle custom-nav-buttons" id="openEditForm" aria-hidden="true" data-target="#editForm" target-discard="#viewForm" title="Edit"></button> -->
            {{-- <button type="button" class="btn btn-pure icon md-delete subtask-toggle" aria-hidden="true" id="deleteDetails" title="Delete"></button> --}}
            <button type="button" class="btn btn-pure slidePanel-close icon md-close" data-dismiss="modal" aria-hidden="true" title="Close"></button>
          </div>
          <h5 class="stage-name taskboard-stage-title">{{ __('admin.owner-view-title') }}</h5>
        </div>
      </div>
      <div class="modal-body custom-nav-tabs">
        <div id="viewForm" class="active">
          <table class="table" >
            <tr>
              <td width="25%"><label class="text-bold">First Name</label></td>
              <td>{{$user->first_name}}</td>
            </tr>
            <tr>
              <td width="25%"><label class="text-bold">Last Name</label></td>
              <td>{{$user->last_name}}</td>
            </tr>
            <tr>
              <td width="25%"><label class="text-bold">Email</label></td>
              <td>{{$user->email}}</td>
            </tr>
            <tr>
              <td width="25%"><label class="text-bold">Mobile</label></td>
              <td>{{$user->mobile_number}}</td>
            </tr>
            <tr>
              <td width="25%"><label class="text-bold">Truck Number</label></td>
              <td>{{$user->Owner->truck_number ?? 'N/A'}}</td>
            </tr>
            <tr>
              <td width="25%"><label class="text-bold">Truck Location</label></td>
              <td>{{$user->Owner->truck_location ?? 'N/A'}}</td>
            </tr>
            <tr>
              <td width="25%"><label class="text-bold">Business Name</label></td>
              <td>{{$user->Owner->business_name ?? 'N/A'}}</td>
            </tr>
            <tr>
              <td width="25%"><label class="text-bold">Signed Up</label></td>
              <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('d M Y') }}</td>
            </tr>
            <tr>
              <td width="25%"><label class="text-bold">Profile Picture</label></td>
              <td>
                <img width="100" style="border-radius: 50%; width: 100px; height: 100px;" class="img-fluid" src="{{asset($user->image)}}">
              </td>
            </tr>
          </table>
        </div>
        <div id="editForm" class="hide">
          <!-- <div class="box-body">
            <div class="col-md-12">
              <div class="row">
                <div class="form-group form-material col-md-6 @error('name') has-danger @enderror">
                  <label class="form-control-label" for="inputBasicFirstName">First Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="First name" value="{{ucfirst($user->first_name)}}" name="name" autocomplete="off" required />
                    @error('name')
                    <label class="form-control-label" for="name">{{ $message }}</label>
                    @enderror
                </div>
              </div>
             </div>
              <br>
              <div class="form-group text-right">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-secondary custom-nav-buttons" target-discard="#editForm" data-target="#viewForm">Close</button>
              </div>
          </div> -->

        </div>
      </div>
    </div>
  </div>

  <!-- /.delete Model-open -->
  <div class="modal fade modal-fade-in-scale-up" id="modalConfirmDelete" aria-hidden="true"
                      aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-sm modal-sm-new">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close deletemodel" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Delete</h4>
          </div>
            <div class="modal-body">
              <p class="delete-conform-p">Are you sure you want to delete?</p>
            </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger deletemodel" >Close</button>
                  <button type="submit" class="btn btn-primary">Delete</button>
                </div>
        </div>
      </div>
    </div>
  <!-- /.delete Model-close -->

  {{ Html::script('themes/admin/assets/js/form-validation.js') }}
    <script type="text/javascript">
      $(document).ready(function() {

        $('.userbirthdate').datepicker({
            todayBtn:'linked',
            format: 'yyyy-mm-dd',
            autoclose:true,
            endDate: "today"
         });


          $('.custom-nav-buttons').click(function(event){
            event.preventDefault();//stop browser to take action for clicked anchor
            //get displaying tab content jQuery selector
            var active_tab_selector = $(this).attr("target-discard");
            //hide displaying tab content
            $(active_tab_selector).removeClass('active');
            $(active_tab_selector).addClass('hide');

            //show target tab content
            var target_tab_selector = $(this).data("target");
            $(target_tab_selector).removeClass('hide');
            $(target_tab_selector).addClass('active');
          });

          if(!$('#image_demo').data('croppie')){
            $image_crop = $('#image_demo').croppie({
              enableExif: true,
              viewport: {
                width:350,
                height:350,
                // type:'square' //circle // rectangular
              },
              boundary:{
                width:600,
                height:400
              }
            });
          }
          else{
            $('#image_demo').data('croppie').destroy();
            $image_crop = $('#image_demo').croppie({
              enableExif: true,
              viewport: {
                width:350,
                height:350,
                // type:'square' //circle // rectangular
              },
              boundary:{
                width:600,
                height:400
              }
            });
          }

          $(document).on('change','#inputGroupFile01',function() {
            var name = $(this).attr('name');
            // var id_name= $(this).attr('id');
            // console.log('name = ' +name);
            var noImage = "{{asset('admin-theme/assets/images/default-img.png')}}";
            var ext = $(this).val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
                alert("Please select only image");
                return false;
            }
            else{
              //$("#file-error").attr("disabled", false);
                /* image crop */
                var reader = new FileReader();
                reader.onload = function (event) {
                  $image_crop.croppie('bind', {
                    url: event.target.result
                  }).then(function(){
                    // console.log('jQuery bind complete');
                  });
                }
                reader.readAsDataURL(this.files[0]);
                $(".crop_image").attr('id',name);
                $('#insertimageModal').modal('show');

            }
            // return false;
            /* EOC image cropping */
          });

          $('.crop_image').click(function(event){

              var className = $(this).attr('id');
            $image_crop.croppie('result', {
              type: 'canvas',
              size: 'viewport'
            }).then(function(response){
              $("#profile-pics-preview").attr("src",response);
              $("#imagebase64").val(response);

              $('#insertimageModal').modal('hide');
              $("#feed_image_error").html("");
            });
          });

          $('.deletemodel').click(function(event){
            $('#modalConfirmDelete').modal('hide');
            });
      });
    </script>
