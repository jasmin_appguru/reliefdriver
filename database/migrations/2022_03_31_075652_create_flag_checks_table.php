<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlagChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flag_checks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('driver_id');
            $table->unsignedBigInteger('owner_id');
            $table->tinyInteger('owner_counter')->default(0);
            $table->tinyInteger('driver_counter')->default(0);
            $table->tinyInteger('accept_counter')->default(0);
            $table->enum('last_flag',['driver','owner','unset'])->default('unset');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flag_checks');
    }
}
