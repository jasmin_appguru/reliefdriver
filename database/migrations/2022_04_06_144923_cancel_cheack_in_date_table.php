<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CancelCheackInDateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_dates', function (Blueprint $table) {
            $table->tinyInteger('is_canceled')->default(0)->comment('conforms = 0 , canceled = 1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_dates', function (Blueprint $table) {
            $table->dropColumn('is_canceled');
        });
    }
}
