<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('availability')->comment('if driver available for all day - so availability status will be every day.')->nullable();
            $table->string('is_inducted_in')->nullable();
            $table->string('inducted_company')->nullable();
            $table->string('industry_experience')->nullable();
            $table->string('day_rate_a')->nullable();
            $table->string('day_rate_b')->nullable();
            $table->string('night_rate_a')->nullable();
            $table->string('night_rate_b')->nullable();
            $table->string('license_class')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
