<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoulamRequestCancels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_cancels', function (Blueprint $table) {
            $table->tinyInteger('owner_counter')->default(1);
            $table->integer('counter')->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_cancels', function (Blueprint $table) {
            $table->dropColumn('owner_counter');
        });
    }
}
