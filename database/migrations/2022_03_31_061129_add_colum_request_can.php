<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumRequestCan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_cancels', function (Blueprint $table) {
            $table->tinyInteger('AcceptCounter')->default(0);

            $table->enum('status',['process','complied'])->default('process');
            //$table->dropConstrainedForeignId('booking_id');
            // $table->dropColumn('booking_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_cancels', function (Blueprint $table) {
            $table->dropColumn('AcceptCounter');
            $table->dropColumn('status');
        });
    }
}
