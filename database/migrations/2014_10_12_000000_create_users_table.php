<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('user_type')->default(1)->comment("1 = Relief Driver,2 = Owner driver");
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('image')->default('default.jpg');
            $table->date('dob')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('otp')->nullable();
            $table->string('email')->unique();
            $table->string('is_profile_setup')->default(0)->comment("0=Not SetUp, 1=SetUp");
            $table->tinyInteger('device_type')->comment("0 = Web, 1 = iOS, 2 = Android");
            $table->string('device_token')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('status')->default('Active');
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
