<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTypeOfAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->decimal('day_rate_a', 10, 2)->change();
            $table->decimal('day_rate_b', 10, 2)->change();
            $table->decimal('night_rate_a', 10, 2)->change();
            $table->decimal('night_rate_b', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->string('day_rate_a')->change();
            $table->string('day_rate_b')->change();
            $table->string('night_rate_a')->change();
            $table->string('night_rate_b')->change();
        });
    }
}
