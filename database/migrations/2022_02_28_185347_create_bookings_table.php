<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('company');
            $table->string('driver_phone');
            $table->string('driver_name');
            $table->string('truck_no');
            $table->string('truckLocation');
            $table->unsignedBigInteger('request_user_id');
            $table->foreign('request_user_id')->references('id')->on('owners');
            $table->unsignedBigInteger('driver_id');
            $table->foreign('driver_id')->references('id')->on('drivers');
            $table->text('instruction');
            $table->enum('shift',['day','night']);
            $table->tinyInteger('is_accept')->default(0)->comment('0 = Pending,1 = Accept, 2 = Cancel owner , 3 = Reject Driver ');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
