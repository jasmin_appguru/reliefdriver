$(document).ready(function() {



    if ($('body.homePage').length) {

        $('.app-screenshot-slider').slick({
            dots: true,
            arrows: false,
            infinite: true,
            speed: 2000,
            autoplay: true,
            autoplaySpeed: 1000,
            //cssEase: 'linear',
            pauseOnHover: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }]
        });


        $('.app-screenshot-slider').slick('slickPause');



        $(window).scroll(function(e) {
            var scrollPos = $(document).scrollTop();
            var appScreenshotPo = $('#app-screenshot').offset().top - 80;
            var appScreenshotBtm = Number($('#app-screenshot').offset().top) + Number($('#app-screenshot').height());

           if (appScreenshotPo == scrollPos || (appScreenshotPo < scrollPos && appScreenshotBtm > scrollPos)) {
              appScreenshot();
            }
        });


        function appScreenshot() {

            if (appScreenshot.added) {
                return;
            }

            $('.app-screenshot-slider').slick('slickPlay');
            appScreenshot.added = true;
        }

    }





    // scroll hide and show

    $('.navbar-nav li a').on('click', function() {
        $('.navbar-collapse').collapse('hide');
    });

    $(".navbar-toggler").click(function() {
        $("body").toggleClass("no-scroll");
    });

    $(".header-menu ul li a").click(function() {
        $("body").removeClass("no-scroll");
    });


    // top nav header scroll active class add start

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > 0) {
            $(".header-main").addClass("active");
        } else {
            $(".header-main").removeClass("active");
        }
    });
    // top nav header scroll active class add end


    // scroll to add class js end

    function onScroll(event) {
        var scrollPos = $(document).scrollTop();
        $('#menu-center li.nav-item a').each(function() {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            
            //console.table(refElement.position().top, scrollPos, refElement.position().top + refElement.height(), scrollPos);

            if (refElement.position().top - 50 <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('#menu-center li.nav-item a').removeClass("active");
                currLink.addClass("active");
            } else {
                currLink.removeClass("active");
            }
        });
    }

    $(document).on("scroll", onScroll);

    //smoothscroll
    $('a[href^="#"]').on('click', function(e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function() {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - 50
        }, 500, 'swing', function() {
            //window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });



});


$(window).on('load',function(){
    setTimeout(function(){ 
    $('.page-loader').fadeOut('slow');
    });
});

