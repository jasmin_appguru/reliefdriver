<?php
return [
    "APP_NAME" => "Relief Driver",
    "LOGO_FAVICON" => "images/favicon.png",
    "FROM_EMAIL" => "info@appgurus.com.au",
    "SUPPORT_EMAIL" => "info@appgurus.com.au",
    "REPLAY_NAME" => "Relief Driver",
    "DATE_FORMAT" => "Y-m-d",
    "DATETIME_FORMAT" => "Y-m-d H:i:s",
    "ADS_TIMEZONE" => "Australia/Queensland",

    "CURRENCY_ICON" => "$",
    "ADMIN_CHARGE" => "5",

    "GOOGLE_MAP_API_KEY" => "AIzaSyBLMAE4YhzLHdK655ZfVJ5ZC2eiGBi0yAk",

    "STRIPE_SECRET_KEY" => "sk_test_rcSQKUkkiJRKdGmaQ5oPNhG900ChDjUnDd",
    "STRIPE_PUBLISH_KEY" => "pk_test_ulN8aShxsxX2K9hU3oleUnJ100D1FTUoBw",

    "ADMIN" => [
        "APP_NAME" => "Relief Driver",
        "HEADER_LOGO" => "themes/logos/logo.png",
        "LOGIN_LOGO" => "themes/logos/logo.png",
    ],

    "WEB" => [
        "APP_NAME" => "Relief Driver",
        "HEADER_LOGO" => "themes/logos/logo.png",
        "FOOTER_LOGO" => "themes/logos/logo.png",
    ],

    "EMAIL" => [
        "THEME_COLOR" => "#f1f1f1",
        "THEME_FONT_COLOR" => "#66E798",
        "LOGO" => "themes/logos/logo_email.png",
    ],

];
