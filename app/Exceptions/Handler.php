<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    // protected function unauthenticated($request, AuthenticationException $exception)
    // {
    //     if ($request->expectsJson()) {
    //         $res = [
    //             'success' => 3,
    //             'message' => "Invalid tokan.",
    //         ];
    //         if(!empty($errorMsg)){
    //             $res['data'] = $errorMsg;
    //         }
    //         return response()->json($res, 401);
    //     }
    // }

    public function render($request, Throwable $e)
    {
        if($request->route()->getPrefix() === 'api')
        {
            if ($e instanceof AuthenticationException) {
                $res = [
                    'status' => 4,
                    'message' => "Invalid Access Tokan.",
                ];
                if(!empty($errorMsg)){
                    $res['data'] = $errorMsg;
                }
                return response()->json($res, 200);
            }
        }
        if($request->route()->getPrefix() === 'admin')
        {
            if ($e instanceof AuthenticationException) {
                return redirect()->route('adminlogin');
            }
        }
        return parent::render($request, $e);
    }
}
