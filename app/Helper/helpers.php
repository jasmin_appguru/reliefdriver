<?php
use App\Models\ApplicationNotification;
use App\Models\Booking;
use App\Models\User;
use App\Models\FlagCheck;
use App\Models\RequestCancel;
use Intervention\Image\Facades\Image as Image;

function cancall_all_same_truck_request($booking,$date)
{
    $other_Booking=Booking::where('request_user_id',$booking->request_user_id)->where('shift',$booking->shift)->where('truck_no',$booking->truck_no)->where('id',"!=",$booking->id)
    ->whereHas('bookingDates',function($q) use($date){
            $q->where('date',$date);
        })
    ->update(['is_accept'=>2]);
}

function changeDateFormate($date,$date_format){
    return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format($date_format);
}

function thumbnail($request,$name)
{
    $file = $request->file($name);
    $extension = $file->getClientOriginalName();
    $username = time().'-user_name.'.$file->getClientOriginalExtension();
    $thumb = Image::make($file->getRealPath())->resize(100, 100, function ($constraint) {
        $constraint->aspectRatio(); //maintain image ratio
    });
    $destinationPath = storage_path('app/public/avatars');
    $file->move( $destinationPath, $username);
    $thumb->save($destinationPath.'/thumb/'.$username);
    return $username;
}


function SendNotification($body,$userid,$push=true,$title="Default")
{
    $notification = new ApplicationNotification();
    if($title=="Default")
    {
        $notification->title=__('api.push_notification_title');
    }
    else
    {
        $notification->title=$title;
    }
    $notification->description=$body;
    $notification->user_id=$userid;
    $notification->save();
    if($push=='true')
    {
        SendPushNotification($userid,$body);
    }
}

function date_formate_notification($date)
{
    if(count($date)>1)
    {
        return date('d M Y',strtotime($date['0']))." - ".date('d M Y',strtotime($date[count($date)-1])).".";
    }
    else
    {
        return date('d M Y',strtotime($date['0'])).".";
    }
}

function SendPushNotification($userid,$massage)
{
    $user=User::find($userid);
    if($user->device_type=='1')
    {
        if(!empty($user->device_token))
        {
            $name=__('api.push_notification_title');
            send_notification_ios($user->device_token,["title"=>$name,"body"=>$massage]);

        }
    }
    else
    {
        if(!empty($user->device_token))
        {
            $name=__('api.push_notification_title');
            send_notification_android($user->device_token,["title"=>$name,"body"=>$massage]);
        }
    }

}


function send_notification_ios($tokens, $message,$bulk=false)
{
	$message['sound']="Default";
    $key=config('relief_driver.FCM_TOKEN');
	$url = 'https://fcm.googleapis.com/fcm/send';
	$singleID = array($tokens);

    if($bulk=="true")
    {
        $singleID="";
        $singleID=$tokens;
    }

	$fields = array (
	'registration_ids' => $singleID,
	'data' => $message,
	'notification' => $message
	);

    // 	echo "<pre>"; print_r($fields); die();
    \Log::debug("IOS Test Debug : ".json_encode($fields));

	$headers = array(
	'Authorization: key=' . $key,
	'Content-Type: application/json'
	);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
    // 	print_r($result); exit("aa");
	if ($result === FALSE)
	{
	//die('Curl failed: ' . curl_error($ch));
	}
	// echo "<pre>"; print_r($fields); die();
	// return json_decode($result);
	curl_close($ch);
	// echo "<pre>";
	// print_r($result); die();
}

function send_notification_android($tokens,$message,$bulk=false)
{
	$key=config('relief_driver.FCM_TOKEN');

    $singleID = array($tokens);

    if($bulk=="true")
    {
        $singleID="";
        $singleID=$tokens;
    }
    // dd($singleID);
	$url = 'https://fcm.googleapis.com/fcm/send';

	$fields = array
				(
					'registration_ids' => $singleID,
					'data' => $message,
				);

                //print_r(env('FCM_KEY')); exit();
    \Log::debug("Android Push Test Debug : ".json_encode($fields));
	$headers = array(
	'Authorization: key='.$key,
	'Content-Type: application/json'
	);
	// print_r($fields); exit();
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);

	if ($result === FALSE)
	{
	die('Curl failed: ' . curl_error($ch));
	}
	// echo "<pre>"; print_r($fields); die();
	//return json_decode($result);
	curl_close($ch);
	// echo "<pre>";
    //	 echo "<pre>";

}


function updateFlage($booking)
{
    return "depricated";
    $book=Booking::find($booking->id);
    \Log::debug("-".$book->id."##".$book->is_accept);
    $flagech=FlagCheck::where('driver_id',$book->driver_id)
    ->where('owner_id',$book->request_user_id)->first();
    if(empty($flagech))
    {
        $flage=new FlagCheck();
        $flage->driver_id=$book->driver_id;
        $flage->owner_id=$book->request_user_id;
        if($book->is_accept=="1")
        {
            $flage->accept_counter=1;
        }
        elseif($book->is_accept=="2")
        {
            $flage->owner_counter=1;
        }
        elseif($book->is_accept=="3")
        {
            $flage->driver_counter=1;
        }
        else{

        }
        $flage->save();

    }
    else{
        if($book->is_accept=="1")
        {
            if($flagech->accept_counter >= 2)
            {
                $flagech->last_flag="unset";
                $flagech->owner_counter=0;
                $flagech->driver_counter=0;
                $flagech->accept_counter=1;
                $flagech->save();
                RequestCancel::where('driver_id',$book->driver_id)->where('owner_id',$book->request_user_id)->delete();
            }
            else
            {
                $flagech->accept_counter=$flagech->accept_counter+1;
                $flagech->save();
            }
        }
        elseif($book->is_accept=="2")
        {
            \Log::debug($flagech->owner_counter);
            if($flagech->owner_counter >= 2)
            {
                $flagech->last_flag="owner";
                //$flagech->accept_counter=0;
                \Log::debug("owner");
                $flagech->owner_counter=$flagech->owner_counter+1;
                $flagech->save();
            }
            else
            {
                $flagech->owner_counter++;
                $flagech->save();
            }

        }
        elseif($book->is_accept=="3")
        {
            if($flagech->driver_counter >= 2)
            {
                $flagech->last_flag="driver";
                \Log::debug("driver");
                $flagech->accept_counter=0;
                $flagech->driver_counter=$flagech->driver_counter+1;
                $flagech->save();
            }
            else
            {
                $flagech->driver_counter=$flagech->driver_counter+1;
                $flagech->save();
            }
        }
        else{

        }


    }
    return "sdd";
}

function updateFlags($booking)
{

    $book=Booking::find($booking->id);
    // if($book->is_accept == "2")
    // {
    //     $book->status="complied";
    //     $book->save();
    //     return false;
    // }

    \Log::debug("-".$book->id."##".$book->is_accept);
    $flagech=FlagCheck::where('driver_id',$book->driver_id)
    ->where('owner_id',$book->request_user_id)->first();
    if(empty($flagech))
    {
        $flage=new FlagCheck();
        $flage->driver_id=$book->driver_id;
        $flage->owner_id=$book->request_user_id;
        if($book->is_accept=="1")
        {
            $flage->accept_counter=1;
        }
        elseif($book->is_accept=="2")
        {
            $flage->owner_counter=1;
        }
        elseif($book->is_accept=="3")
        {
            $flage->driver_counter=1;
        }
        else{

        }
        $flage->save();

    }
    else{
        if($book->is_accept=="1")
        {
            if($flagech->accept_counter >= 2)
            {
                if($flagech->last_flag=="owner")
                {
                    $flagech->owner_counter=0;
                }
                else
                {
                    $flagech->driver_counter=0;
                }
                $flagech->last_flag="unset";
                $flagech->accept_counter=0;
                $flagech->save();
                RequestCancel::where('driver_id',$book->driver_id)->where('owner_id',$book->request_user_id)->delete();
            }
            else
            {
                $flagech->accept_counter=$flagech->accept_counter+1;
                $flagech->save();
            }
        }
        elseif($book->is_accept=="2")
        {
            \Log::debug($flagech->owner_counter);
            if($flagech->owner_counter >= 2)
            {
                $flagech->last_flag="owner";
                //$flagech->accept_counter=0;
                \Log::debug("owner");
                $flagech->owner_counter=$flagech->owner_counter+1;
                $flagech->save();
            }
            else
            {
                $flagech->owner_counter++;
                $flagech->save();
            }

        }
        elseif($book->is_accept=="3")
        {
            if($flagech->driver_counter >= 2)
            {
                $flagech->last_flag="driver";
                \Log::debug("driver");
                $flagech->accept_counter=0;
                $flagech->driver_counter=$flagech->driver_counter+1;
                $flagech->save();
            }
            else
            {
                $flagech->driver_counter=$flagech->driver_counter+1;
                $flagech->save();
            }
        }
        else{

        }

    }
    $book->status="complied";
    $book->save();
    return "sdd";
}

function CompanyUi($compny_name)
{
    // Boral #16A851
    // Hanson #002E46
    // Holcim #FF1100
    // Wagners#FFE411
    // Q-Crete#A10D34
    // Hymix#FED02E
    // The Neilsen Group#108865
    // Cordwell’s Concreate#1120A3
    // Hy-Tec#E58720
    $compny_name=trim($compny_name);
    if($compny_name=='Boral')
    {
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round' style='background-color: #16a851 !important;'></div>".$compny_name."</div>";
    }
    elseif($compny_name=='Hanson')
    {
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round' style='background-color: #002E46 !important;'></div>".$compny_name."</div>";
    }
    elseif($compny_name=='Holcim')
    {
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round' style='background-color: #FF1100 !important;'></div>".$compny_name."</div>";
    }
    elseif($compny_name=='Wagners')
    {
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round' style='background-color: #FFE411 !important;'></div>".$compny_name."</div>";
    }
    elseif($compny_name=='Q-Crete' || $compny_name=='Q-crete')
    {
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round' style='background-color: #A10D34 !important;'></div>".$compny_name."</div>";
    }
    elseif($compny_name=='Hymix')
    {
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round' style='background-color: #FED02E !important;'></div>".$compny_name."</div>";
    }
    elseif($compny_name=='The Neilsen Group')
    {
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round' style='background-color: #108865 !important;'></div>".$compny_name."</div>";
    }
    elseif($compny_name=='Cordwell’s Concreate')
    {
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round' style='background-color: #1120A3 !important;'></div>".$compny_name."</div>";
    }
    elseif($compny_name=='Hy-Tec')
    {
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round' style='background-color: #E58720 !important;'></div>".$compny_name."</div>";
    }
    elseif($compny_name=='Mansels')
    {
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round' style='background-color: #f0c137 !important;'></div>".$compny_name."</div>";
    }
    else{
        return "<div style='display: flex; margin-right: 5px; padding-bottom: 5px;'><div class='round'></div>".$compny_name."</div>";
    }

}

function DateUi($dates)
{
    $html="";
    $last_change="";
    foreach($dates as $date)
    {
            $last_date=\Carbon\Carbon::parse($date->date)->format('M Y');
            if($last_change!==$last_date)
            {
                $html.="</div><span style='margin-top: 3px; margin-right: 3px; margin-right: 3px; padding: 4px; border-radius: 10px; background: #777777; color: #fff;'>
                        ".\Carbon\Carbon::parse($date->date)->format('M Y')."</span><br><div class='flax' style='display: flex; margin-top: 6px; margin-bottom: 6px'>";
            }
            $html.="<div class='round' style='background-color: #66e798 !important; color:#fff; padding-left: 3px; '>".\Carbon\Carbon::parse($date->date)->format('d')."</div>";;
            $last_change=$last_date;
    }
                    return $html."</div>";
    // if($compny_name=='Boral')
    // {
    //     return "<div style='    display: flex;'><div class='round' style='background-color: #16a851 !important;'></div>".$compny_name."</div>";
    // }
    // elseif($compny_name=='Hanson')
    // {
    //     return "<div style='    display: flex;'><div class='round' style='background-color: #002E46 !important;'></div>".$compny_name."</div>";
    // }
    // elseif($compny_name=='Holcim')
    // {
    //     return "<div style='    display: flex;'><div class='round' style='background-color: #FF1100 !important;'></div>".$compny_name."</div>";
    // }
    // elseif($compny_name=='Wagners')
    // {
    //     return "<div style='    display: flex;'><div class='round' style='background-color: #FFE411 !important;'></div>".$compny_name."</div>";
    // }
    // elseif($compny_name=='Q-Crete')
    // {
    //     return "<div style='    display: flex;'><div class='round' style='background-color: #A10D34 !important;'></div>".$compny_name."</div>";
    // }
    // elseif($compny_name=='Hymix')
    // {
    //     return "<div style='    display: flex;'><div class='round' style='background-color: #FED02E !important;'></div>".$compny_name."</div>";
    // }
    // elseif($compny_name=='The Neilsen Group')
    // {
    //     return "<div style='    display: flex;'><div class='round' style='background-color: #108865 !important;'></div>".$compny_name."</div>";
    // }
    // elseif($compny_name=='Cordwell’s Concreate')
    // {
    //     return "<div style='    display: flex;'><div class='round' style='background-color: #1120A3 !important;'></div>".$compny_name."</div>";
    // }
    // elseif($compny_name=='Hy-Tec')
    // {
    //     return "<div style='    display: flex;'><div class='round' style='background-color: #E58720 !important;'></div>".$compny_name."</div>";
    // }
    // else{
    //     return "<div style='    display: flex;'><div class='round'></div>".$compny_name."</div>";
    // }

}



