<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class DriverExport implements FromCollection,WithHeadings,WithMapping,WithColumnFormatting
{
    protected $sr;

    public function __construct()
    {
        $this->sr = 0;
    }
    public function collection()
    {
        return User::where('user_type',1)->with('Driver')->get();
    }

    public function map($user): array
    {
        $this->sr++;
        return [
            $this->sr,
            $user->first_name,
            $user->last_name,
            $user->email,
            (string)$user->mobile_number,
            implode(',',array_unique(explode(",",$user->Driver->availability))),
            (string)$user->Driver->industry_experience,
            $user->Driver->day_rate_a,
            $user->Driver->day_rate_b,
            $user->Driver->night_rate_a,
            $user->Driver->night_rate_b,
            implode(',',array_unique(explode(",",$user->Driver->license_class))),
        ];
    }

    public function headings(): array
    {
        return [
            'Sr. No',
            'First Name',
            'Last Name',
            'Email',
            'Mobile',
            'Availability',
            'Experience (Years)',
            '($) Day A',
            '($) Day B',
            '($) Night A',
            '($) Night B',
            'License Class'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'E' => "+#",
            'G' => "+#",
        ];
    }
}
