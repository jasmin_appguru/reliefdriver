<?php

namespace App\Exports;

use App\Models\User;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AllAgentsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //return User::all();

        DB::statement(DB::raw('set @row=0'));
        $data = User::selectRaw('@row:=@row+1 as srno, first_name, last_name, email,mobile_number')->get();
        return $data;

    }

    public function headings(): array
    {
        return [
            'Sr.No',
            'First Name',
            'Last Name',
            'Email',
            'Mobile',
            'Agency Name',
            'Promo Code'
        ];
    }
}
