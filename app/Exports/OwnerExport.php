<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use DB;
use App\Models\User;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OwnerExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $sr;

    public function __construct()
    {
        $this->sr = 0;
    }
    public function collection()
    {
        return User::where('user_type',2)->with('Owner')->get();
    }

    public function map($user): array
    {
        $this->sr++;
        return [
            $this->sr,
            $user->first_name,
            $user->last_name,
            $user->email,
            (string)$user->mobile_number,
            $user->Owner->business_name,
            $user->Owner->truck_number,
            $user->Owner->truck_location,
        ];
    }

    public function headings(): array
    {
        return [
            'Sr. No',
            'First Name',
            'Last Name',
            'Email',
            'Mobile',
            'Business Name',
            'Truck Number',
            'Truck Location',
        ];
    }
}
