<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if($this->user_type==1)
        {
            $id = $this->driver->id;
        }
        else
        {
            $id = $this->owner->id;
        }

        $responce=[
            'userId' => $id,
            'firstName' => $this->first_name ?? '',
            'lastName' => $this->last_name ?? '',
            'mobileNumber' => $this->mobile_number,
            'userProfileImg' => $this->image,
            'thumbnail' => $this->thumbnail,
            'email' => $this->email,
            'userType' => (int)$this->user_type,
            'token' => $this->tokan ?? '-',
            'isProfileSetUp' => (int)$this->is_profile_setup ?? 0,
            'isSubscribed' => $this->IsSubscribed,
            'expiryDate' => $this->ExpireAt ?? '',
        ];

        if($this->user_type==1)
        {
            $responce['truckNumber']="";
            $responce['businessName']="";
            $responce['truckLocation']="";
            $responce['latitude']="";
            $responce['longitude']="";
        }
        else{
            $responce['truckNumber']=$this->owner->truck_number ?? "";
            $responce['businessName']=$this->owner->business_name ?? "";
            $responce['truckLocation']=$this->owner->truck_location ?? "";
            $responce['latitude']=$this->owner->latitude ?? "";
            $responce['longitude']=$this->owner->longitude ?? "";
        }

        return $responce;

    }
}
