<?php

namespace App\Http\Resources\Driver;

use Illuminate\Http\Resources\Json\JsonResource;

class CompleteProfileDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "isProfileSetUp" => 1,
            'firstName' => $this->first_name ?? 0,
            'lastName' => $this->last_name ?? 0,
            'email' => $this->email ?? 0,
            'userProfileImg' => $this->image,
            'thumbnail' => $this->thumbnail,
            'Dob' => $this->dob ? date('d M Y',strtotime($this->dob)) : "",
            'availabilityStatus' => explode(",",$this->Driver->availability) ?? "0",
            'isInductedIn' => $this->Driver->is_inducted_in ?? 0,
            'inductedCompany' => $this->Driver->inducted_company ?? 0,
            'industryExperience' => $this->Driver->industry_experience ?? 0,
            'dayRateA' => (string)round($this->Driver->day_rate_a) ?? 0,
            //'dayRateB' => (string)round($this->Driver->day_rate_b) ?? 0,
            'nightRateA' => (string)round($this->Driver->night_rate_a) ?? 0,
            //'nightRateB' => (string)round($this->Driver->night_rate_b) ?? 0,
            'licenceClass' => $this->Driver->license_class ?? 0,
            'hasAvailableDays' => $this->Driver->hasAvailableDays ?? "false",
            'unavailableDates' => UnAvailableDatesResource::collection($this->Driver->UnAvailableDates),
            'isSubscribed' => $this->IsSubscribed,
            'expiryDate' => $this->ExpireAt ?? '',
        ];
        return parent::toArray($request);
    }
}
