<?php

namespace App\Http\Resources\Driver;

use Illuminate\Http\Resources\Json\JsonResource;

class EditProfileDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "userId" =>  $this->Driver->id,
            "userProfileImg" => $this->image,
            'thumbnail' => $this->thumbnail,
            "firstName" => $this->first_name,
            "lastName" => $this->last_name,
            "email" => $this->email,
            "Dob" => $this->dob,
            "mobileNumber" => $this->mobile_number,
            "hasAvailableDays" =>$this->Driver->hasAvailableDays ?? 'false',
            "availabilityStatus" => explode(",",$this->Driver->availability),
            "isInductedIn"=> $this->Driver->is_inducted_in,
            "inductedCompany" => $this->Driver->inducted_company,
            "industryExperience" => $this->Driver->industry_experience,
            "dayRateA" => (string)round($this->Driver->day_rate_a),
            //"dayRateB" => (string)round($this->Driver->day_rate_b),
            "nightRateA" => (string)round($this->Driver->night_rate_a),
            //"nightRateB" => (string)round($this->Driver->night_rate_b),
            "licenceClass" => $this->Driver->license_class,
            'unavailableDates' => UnAvailableDatesResource::collection($this->Driver->UnAvailableDates),
        ];
    }
}
