<?php

namespace App\Http\Resources\Notification;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'notificationId' => $this->id,
            'notificationText' => $this->description,
            'notificationTime' =>  \Carbon\Carbon::parse($this->created_at)->format('d M Y h:i A'),
            'isRead' => ($this->is_read==0)?"false":"true",
        ];
    }
}
