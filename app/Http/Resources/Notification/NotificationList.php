<?php

namespace App\Http\Resources\Notification;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'notificationId' => $this->id,
            'notificationText' => $this->title,
            'notificationTime' =>  \Carbon\Carbon::parse($this->crated_at)->format('M d Y'),
            'isRead' => $this->is_read,
        ];
        return parent::toArray($request);
    }
}
