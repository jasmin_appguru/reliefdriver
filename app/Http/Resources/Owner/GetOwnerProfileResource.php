<?php

namespace App\Http\Resources\Owner;

use Illuminate\Http\Resources\Json\JsonResource;

class GetOwnerProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "userProfileImg" => $this->image ?? "",
            'thumbnail' => $this->thumbnail ?? "",
            "businessName" => $this->owner->business_name ?? "",
            "firstName" => $this->first_name,
            "lastName" => $this->last_name,
            "email" => $this->email,
            "Dob" => date('d M Y', strtotime($this->dob)),
            "mobileNumber" => $this->mobile_number,
            "truckNumber" => $this->owner->truck_number,
            "truckLocation" => $this->owner->truck_location,
            "latitude" => $this->owner->latitude,
            "longitude" => $this->owner->longitude
        ];
        return parent::toArray($request);
    }
}
