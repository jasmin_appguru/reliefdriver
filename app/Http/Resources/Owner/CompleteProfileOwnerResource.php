<?php

namespace App\Http\Resources\Owner;

use Illuminate\Http\Resources\Json\JsonResource;

class CompleteProfileOwnerResource extends JsonResource
{
    public function toArray($request)
    {

        return [
            "isProfileSetUp" => 1,
            "userId" => $this->Owner->id,
            "userType" => $this->user_type,
            "userProfileImg" => $this->image,
            'thumbnail' => $this->thumbnail,
            "businessName" => $this->owner->business_name,
            "firstName" => $this->first_name,
            "lastName" => $this->last_name,
            "email" => $this->email,
            "Dob" => date('d M Y',strtotime($this->dob)),
            "mobileNumber" => $this->mobile_number,
            "truckNumber" => $this->owner->truck_number,
            "truckLocation" => $this->owner->truck_location,
            "latitude" => $this->owner->latitude,
            "longitude" => $this->owner->longitude,
            'isSubscribed' => $this->IsSubscribed,
            'expiryDate' => $this->ExpireAt ?? '',
        ];
        return parent::toArray($request);
    }
}
