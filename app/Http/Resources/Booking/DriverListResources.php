<?php

namespace App\Http\Resources\Booking;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverListResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "userProfileImg" => $this->user->image,
            "reliefDriverId" => $this->id,
            "firstName" => $this->user->first_name,
            "lastName" => $this->user->last_name,
            "industryExperience" => $this->industry_experience ?? "",
            "dayRateA" => (string)round($this->day_rate_a),
            //"dayRateB" => (string)round($this->day_rate_b),
            "nightRateA" => (string)round($this->night_rate_a),
            //"nightRateB" => (string)round($this->night_rate_b),
            "inductedCompany" => $this->inducted_company ?? "",
            "licenceClass" => $this->license_class,
            'Dob' => ($this->user->dob) ? date('d M Y',strtotime($this->user->dob)) : "",
            "mobileNumber" => $this->user->mobile_number,
            "email" => $this->user->email,
            "availabilityStatus" => explode(",",$this->availability) ?? "0",
        ];
        return parent::toArray($request);
    }
}
