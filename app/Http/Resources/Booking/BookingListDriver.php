<?php

namespace App\Http\Resources\Booking;

use Illuminate\Http\Resources\Json\JsonResource;

class BookingListDriver extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "ownerCompany" => $this->Owner->business_name,
            "ownerDriverName" => $this->Owner->User->first_name ?? "",
            "driverName" => $this->Driver->User->first_name ?? "",
            "ownerDriverPhoneNumber" => $this->Owner->User->mobile_number,
            "driverPhoneNumber" => $this->Driver->User->mobile_number,
            "requestedUserId" => $this->driver_id,
            "bookingId" => $this->id,
            "truckNo" => $this->truck_no,
            "ownerDriverId" => $this->request_user_id,
            "truckLocation" => $this->truckLocation,
            "instruction" => $this->instruction ?? '',
            "shift" => $this->shift,
            "latitude" => $this->latitude ?? '',
            "longitude" => $this->longitude ?? '',
            "bookingDates" => BookingDatesResource::collection($this->bookingDates)
        ];
        return parent::toArray($request);
    }
}
