<?php

namespace App\Http\Resources\Booking;

use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //requestedUserId that means who send request
        return [
            "bookingId" => $this->id,
            "ownerCompany" => $this->Owner->business_name,
            "ownerDriverName" => $this->Owner->User->first_name,
            "driverName" => $this->Driver->User->first_name,
            "ownerDriverPhoneNumber" => $this->Owner->User->mobile_number,
            "driverPhoneNumber" => $this->Driver->User->mobile_number,
            "ownerDriverId" => $this->request_user_id,
            "requestedUserId" => $this->driver_id,
            "truckNo" => $this->truck_no,
            "truckLocation" => $this->truckLocation,
            "instruction" => $this->instruction,
            "shift" => $this->shift,
            "latitude" => $this->latitude,
            "longitude" => $this->longitude,
            "bookingDates" => BookingDatesResource::collection($this->bookingDates)
        ];
        // ownerCompany
        return parent::toArray($request);
    }
}
