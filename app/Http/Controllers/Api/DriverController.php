<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Driver\CompleteProfileDriverResource;
use App\Http\Resources\Driver\GetDriverProfile;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Resources\Driver\EditProfileDriverResource;
use Illuminate\Http\Request;
use App\Models\Driver;
use App\Models\Booking;
use App\Models\OtpManager;
use App\Models\DriverUnavailableDate;
use Validator;
use App\Models\User;
class DriverController extends BaseController
{
    /**
     * update the profile build relation and add new Recode
     * @param userId,userType,userProfileImg,firstName,lastName,email,Dob,mobileNumber,truckNumber,truckLocation,deviceToken
     * @return isProfileSet,userId,userType,userProfileImg,businessName,firstName,lastName,email,Dob,mobileNumber,truckNumber,truckLocation
     */
    public function completeProfile(Request $request)
    {
        $role=[
            'userId' => 'required',
            'userType' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'Dob' => 'required|date',
            'availabilityStatus' =>  'required|max:100|json',
            'hasAvailableDays' =>  'required',
            'unavailableDates' => 'required|json',
            'isInductedIn' =>  'required',
            'industryExperience' =>  'required',
            'dayRateA' =>  'required|numeric',
            'nightRateA' =>  'required|numeric',
            'licenceClass' =>  'required|max:100',
            'email' => 'required|email',
            'mobileNumber' => 'required|min:10',
        ];

        

        if($request->has("isInductedIn") && $request->isInductedIn=="Yes")
        {
            $role['inductedCompany'] = 'required';
        }

        if($request->user()->mobile_number != $request->mobileNumber)
        {
            $role['otp'] = 'required';
        }
        $validator = Validator::make($request->all(),$role,[
            'email.unique' => "You are already registered with this email address. Please login to proceed."
        ]);
        //for validation request

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if($request->user()->mobile_number != $request->mobileNumber)
        {
            if($user = User::where('mobile_number',$request->mobileNumber)->where('user_type',$request->userType)->first())
            {
                return $this->handleError(__('api.register_mobile_number_unique'));
            }
        }
        //for errors of request

        if($request->has('otp') && $request->otp!="")
        {
            if(!$this->_validateOTP($request->mobileNumber,$request->otp))
            {
                return $this->handleError(__('api.invalid_otp'));
            }
        }

        $user=\Auth::user();

        if($this->CheckDriverArability(json_decode($request->unavailableDates),$user->driver->id))
        {
            return $this->handleError(__('api.driver_edit_already_booking'));
        }
        $user->first_name=$request->firstName;
        $user->last_name=$request->lastName;
        $user->dob=date('Y-m-d',strtotime($request->Dob));
        $user->mobile_number=$request->mobileNumber;
        $user->is_profile_setup=1;
        if(!empty($request->file('userProfileImg'))){
            // $request->file('userProfileImg')
            // $path = $request->file('userProfileImg')->store(
            //     'avatars', 'public'
            // );
            // $user->image=$path;
            $user->image=thumbnail($request,'userProfileImg');
        }
        $user->save();
        // $user->hasAvailableDays=$request->hasAvailableDays;
        $res=DriverUnavailableDate::where('driver_id',$user->driver->id)->delete();
        $driver=Driver::updateOrCreate(
            ['user_id' => $user->id],
            [
                'availability' => implode(',',json_decode($request->availabilityStatus)),
                'is_inducted_in' => $request->isInductedIn,
                'inducted_company' => $request->inductedCompany ?? "",
                'industry_experience' => $request->industryExperience,
                'day_rate_a' => $request->dayRateA,
                'night_rate_a' => $request->nightRateA,
                'hasAvailableDays' => $request->hasAvailableDays,
                'license_class' => $request->licenceClass,
            ]
        );

        foreach(json_decode($request->unavailableDates) as $data)
        {
            $res=DriverUnavailableDate::updateOrCreate(
                ['date' => date('Y-m-d',strtotime($data)), 'driver_id' => $driver->id]
            , ['driver_id'=>$driver->id]);
        }

        \Auth::user()->WelcomEmail();

        return $this->handleResponse(new CompleteProfileDriverResource($user), 'Driver Profile Setup successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param userId,token,userProfileImg,businessName,firstName,lastName,dob,mobileNumber,truckLocation,latitude,longitude,deviceToken
     * @return isProfileSet,userProfileImg,businessName,firstName,lastName,dob,mobileNumber,truckLocation
     */

    public function editProfileDriver(Request $request)
    {
        $role=[
            'userId' => 'required',
            'userType' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'Dob' => 'required|date',
            'hasAvailableDays' =>  'required',
            'availabilityStatus' =>  'required|json',
            'unavailableDates' => 'required|json',
            'isInductedIn' =>  'required',
            'industryExperience' =>  'required',
            'dayRateA' =>  'required|numeric',
            'nightRateA' =>  'required|numeric',
            'licenceClass' =>  'required|max:100',
            'mobileNumber' => 'required|min:10'
        ];

        if($request->has("isInductedIn") && $request->isInductedIn=="Yes")
        {
            $role['inductedCompany'] = 'required';
        }

        if($request->user()->mobile_number != $request->mobileNumber)
        {
            $role['otp'] = 'required';
        }

        $validator = Validator::make($request->all(),$role);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if(\Auth::user()->is_profile_setup==0)
        {
            return $this->handleError('Your Profile is not set yet Please setup profile after that you can edit.',[]);
        }

        if($request->user()->mobile_number != $request->mobileNumber)
        {
            if($user = User::where('mobile_number',$request->mobileNumber)->where('user_type',$request->userType)->first())
            {
                return $this->handleError(__('api.register_mobile_number_unique'));
            }
        }

        if($request->has('otp') && $request->otp!="")
        {
            if(!$this->_validateOTP($request->mobileNumber,$request->otp))
            {
                return $this->handleError(__('api.invalid_otp'));
            }
        }

        $user=\Auth::user();
        $user->first_name=$request->firstName;
        $user->last_name=$request->lastName;
        $user->dob=date('Y-m-d',strtotime($request->Dob));
        $user->mobile_number=$request->mobileNumber;
        if(!empty($request->file('userProfileImg'))){
            // $path = $request->file('userProfileImg')->store(
            //     'avatars', 'public'
            // );
            // $user->image=$path;
            $user->image=thumbnail($request,'userProfileImg');
        }
        $user->save();



        $user->driver;
        $user->driver->availability = implode(',',json_decode($request->availabilityStatus));
        $user->driver->is_inducted_in = $request->isInductedIn;
        $user->driver->inducted_company = $request->inductedCompany;
        $user->driver->industry_experience = $request->industryExperience;
        $user->driver->day_rate_a = $request->dayRateA;
        $user->driver->night_rate_a = $request->nightRateA;
        $user->driver->hasAvailableDays = $request->hasAvailableDays;
        $user->driver->license_class = $request->licenceClass;
        $user->driver->save();

        //foreach(explode(',',json_decode($request->unavailableDates)) as $data)
        if($this->CheckDriverArability(json_decode($request->unavailableDates),$user->driver->id))
        {
            return $this->handleError(__('api.driver_edit_already_booking'));
        }
        $res=DriverUnavailableDate::where('driver_id',$user->driver->id)->delete();

        foreach(json_decode($request->unavailableDates) as $data)
        {
            //$res=DriverUnavailableDate::where('driver_id',$user->driver->id)->delete();
            $res=DriverUnavailableDate::updateOrCreate(
                ['date' => date('Y-m-d',strtotime($data)), 'driver_id' => $user->driver->id]
            , ['driver_id'=>$user->driver->id]);
        }

        return $this->handleResponse(new EditProfileDriverResource($user), 'Driver Profile Edit successfully');

    }

    /**
     * Show the form for editing the specified resource.
     * @param  token
     * @return userProfileImg,firstName,lastName,email,Dob,mobileNumber,availabilityStatus,isInductedIn,inductedCompany,industryExperience,dayRateA,dayRateB,nightRateA,nightRateB,licenceClass
     */
    public function getDriverProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $driver=Driver::find($request->userId);
        if(empty($driver))
        {
            return $this->handleError("Invalid Driver Id.");
        }
        return $this->handleResponse(new GetDriverProfile($driver->user), 'Driver profile get successfully.');
    }

    public function _validateOTP($receiverNumber,$otp)
    {
        $otp_manager=OtpManager::where('mobile_number',$receiverNumber)->where('otp',$otp)->orderBy('id','DESC')->first();
        if($otp_manager)
        {
            $otp_manager->otp="";
            $otp_manager->save();
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public function CheckDriverArability($Dates,$driverId)
    {
        foreach($Dates as $date)
        {
            // $dateResulr=DriverUnavailableDate::where('date',date('Y-m-d',strtotime($date)))->where('driver_id',$driverId)->first();
            // if(!empty($dateResulr))
            // {
            //     return true;
            // }
            // is_accept
            $bookingResult=Booking::where('is_accept',1)->where('driver_id',$driverId)->whereHas('bookingDates',function($q) use ($date){
                $q->where('date',date('Y-m-d',strtotime($date)));
            })->first();

            if(!empty($bookingResult))
            {
                return true;
            }
        }

        // $users = User::whereHas('posts', function($q){
        //     $q->where('created_at', '>=', '2015-01-01 00:00:00');
        // })->get();

        return false;
    }
}
