<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Driver\GetDriverProfile;
use App\Http\Resources\Owner\CompleteProfileOwnerResource;
use App\Http\Resources\Owner\GetOwnerProfileResource;
use App\Http\Resources\Booking\DriverListResources;
use App\Models\Owner;
use App\Models\Driver;
use App\Models\OtpManager;
use Auth;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;

class OwnerController extends BaseController
{
    public function completeProfile(Request $request)
    {
        $role= [
            'userId' => 'required',
            'userType' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'Dob' => 'required|date',
            'businessName' => 'required',
            'truckNumber' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'truckLocation' => 'required|min:5|max:100',
            'email' => 'required|email',
            'mobileNumber' => 'required|min:10',
        ];
        if(\Auth::user()->mobile_number!=$request->mobileNumber)
        {
            $role['otp']='required';
        }
        $validator = Validator::make($request->all(), $role,[
            'email.unique' => "You are already registered with this email address. Please login to proceed."
        ]);
        //for validation request
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if($request->user()->mobile_number != $request->mobileNumber)
        {
            if($user = User::where('mobile_number',$request->mobileNumber)->where('user_type',$request->userType)->first())
            {
                return $this->handleError(__('api.register_mobile_number_unique'));
            }
        }

        if($request->has('otp') && $request->otp!="")
        {
            if(!$this->_validateOTP($request->mobileNumber,$request->otp))
            {
                return $this->handleError(__('api.invalid_otp'));
            }
        }
        //for errors of request

        $user=\Auth::user();
        $user->first_name=$request->firstName;
        $user->last_name=$request->lastName;
        $user->dob=date('Y-m-d',strtotime($request->Dob));
        $user->mobile_number=$request->mobileNumber;
        $user->is_profile_setup=1;
        if(!empty($request->file('userProfileImg'))){
            // $path = $request->file('userProfileImg')->store(
            //     'avatars', 'public'
            // );
            // $user->image=$path;
            $user->image=thumbnail($request,'userProfileImg');
        }
        $user->save();

        $owner=Owner::updateOrCreate(
            ['user_id' => $user->id],
            [
                'business_name' => $request->businessName,
                'truck_number' => $request->truckNumber,
                'truck_location' => $request->truckLocation,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
            ]
        );

        \Auth::user()->WelcomEmail();

        return $this->handleResponse(new CompleteProfileOwnerResource($user), 'Owner Profile Setup successfully');
    }

    public function editProfileOwner(Request $request)
    {

        $role= [
            'userId' => 'required',
            'userType' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'Dob' => 'required|date',
            'truckNumber' => 'required',
            'businessName' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'truckLocation' => 'required|min:5|max:100',
            'mobileNumber' => 'required|min:10',
        ];
        if(\Auth::user()->mobile_number != $request->mobileNumber)
        {
            $role['otp']='required';
        }
        $validator = Validator::make($request->all(), $role);
        //for validation request
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if($request->user()->mobile_number != $request->mobileNumber)
        {
            if($user = User::where('mobile_number',$request->mobileNumber)->where('user_type',$request->userType)->first())
            {
                return $this->handleError(__('api.register_mobile_number_unique'));
            }
        }

        if($request->has('otp') && $request->otp!="")
        {
            if(!$this->_validateOTP($request->mobileNumber,$request->otp))
            {
                return $this->handleError(__('api.invalid_otp'));
            }
        }

        //for errors of request
        if(\Auth::user()->is_profile_setup==0)
        {
            return $this->handleError('Your Profile is not set yet Please setup profile after that you can edit.',[]);
        }

        $user=\Auth::user();
        $user->first_name=$request->firstName;
        $user->last_name=$request->lastName;
        $user->dob=date('Y-m-d',strtotime($request->Dob));
        $user->mobile_number=$request->mobileNumber;
        if(!empty($request->file('userProfileImg'))){
            // $path = $request->file('userProfileImg')->store(
            //     'avatars', 'public'
            // );
            // $user->image=$path;
            $user->image=thumbnail($request,'userProfileImg');
        }
        $user->save();

        $owner=Owner::updateOrCreate(
            ['user_id' => $user->id],
            [
                'business_name' => $request->businessName,
                'latitude' => $request->latitude,
                'truck_number' => $request->truckNumber,
                'longitude' => $request->longitude,
                'truck_location' => $request->truckLocation,
            ]
        );

        return $this->handleResponse(new CompleteProfileOwnerResource($user), 'Owner Profile Edit successfully');
    }

    public function getOwnerProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $owner=Owner::find($request->userId);
        if(empty($owner))
        {
            return $this->handleError("Invalid Owner Id.");
        }
        return $this->handleResponse(new GetOwnerProfileResource($owner->user), 'Owner profile get successfully.');
    }

    public function getReliefDriverList(Request $request)
    {
        $user=Driver::whereHas('User', function($q){
            $q->where('is_profile_setup', '=', '1');
        })->where('hasAvailableDays','true')->get();
        return $this->handleResponse(DriverListResources::collection($user), 'Get Driver Profile List');
    }

    public function _validateOTP($receiverNumber,$otp)
    {
        $otp_manager=OtpManager::where('mobile_number',$receiverNumber)->where('otp',$otp)->orderBy('id','DESC')->first();
        if($otp_manager)
        {
            $otp_manager->otp="";
            $otp_manager->save();
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
