<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Resources\Notification\NotificationListResource;
use App\Models\ApplicationNotification;
use Illuminate\Http\Request;
use Validator;

class NotificationController extends BaseController
{
    /**
     * List of all Notification
     *
     * @param  $tokan
     * @return \Illuminate\Http\Response\Json
     */
    public function GetNotificationList(Request $request)
    {
        $Notification=ApplicationNotification::orderBy('id','DESC')->where('user_id',\Auth::user()->id)->get();
        return $this->handleResponse(NotificationListResource::collection($Notification), 'Notifications List fetch successfully');
    }

    public function NotificationRead(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'notificationId' => 'required',
        ]);
        //for validation request

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        $Notification=ApplicationNotification::where('id',$request->notificationId)->first();
        $Notification->is_read=1;
        $Notification->save();
        return $this->handleResponse([], 'Notifications Read successfully');
    }
}
