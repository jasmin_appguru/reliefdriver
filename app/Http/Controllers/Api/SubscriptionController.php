<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use App\Models\MemberShip;

class SubscriptionController extends BaseController
{
    public function GetSubscription(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'receiptData' => 'required',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        //https://buy.itunes.apple.com/verifyReceipt
        $response = Http::post('https://buy.itunes.apple.com/verifyReceipt',[
        //$response = Http::post('https://sandbox.itunes.apple.com/verifyReceipt',[
            'receipt-data' => $request->receiptData,
            'password' => "7d6dcb130f094da29ecd06179989745e",
            'exclude-old-transactions' => 'true',
        ]);
        // dd($response->object()->latest_receipt_info);
        if(isset($response->object()->latest_receipt_info))
        {
            \Log::info(json_encode($response->object()));
            $data=$response->object()->latest_receipt_info;
            \Log::info(json_encode($data));
            // dd($data[count($response->object()->latest_receipt_info)-1]->expires_date);
            $start_date=date('Y-m-d h:i:s',strtotime($data[count($response->object()->latest_receipt_info)-1]->purchase_date));
            $expir_date=date('d/m/Y h:i:s',strtotime($data[count($response->object()->latest_receipt_info)-1]->expires_date));
            $data_res=array();
            $data_res['actual_obj']=$data[count($response->object()->latest_receipt_info)-1];
            $data_res['act_date']=$data[count($response->object()->latest_receipt_info)-1]->expires_date;
            $data_res['start_date']=$start_date;
            $data_res['final_date']=$expir_date;
            // $expir_date
            if ($data[count($response->object()->latest_receipt_info)-1]->product_id=="relief.weeklypay") {

                $data_res['result']='Date is Active';
                $this->CreateSubscription(\Auth::user(),$data[count($response->object()->latest_receipt_info)-1]->product_id,$data[count($response->object()->latest_receipt_info)-1]->transaction_id,'0',date('Y-m-d  H:i:s',strtotime($data[count($response->object()->latest_receipt_info)-1]->purchase_date)),date('Y-m-d  H:i:s',strtotime($data[count($response->object()->latest_receipt_info)-1]->expires_date)));
                // $startDate = strtotime(date('Y-m-d h:i:s', strtotime(\Auth::user()->ExpireAt) ) );
                $start_date=strtotime(\Carbon\Carbon::createFromFormat('d/m/Y H:i:s',\Auth::user()->ExpireAt)->format('Y-m-d H:i:s'));

                \Log::info(date('Y-m-d h:i:s'));
                \Log::info(json_encode($start_date));
                $currentDate = strtotime(date('Y-m-d h:i:s'));
                \Log::info(json_encode($currentDate));
                $res=1;
                if($start_date > $currentDate) {
                    $res=0;
                }

                return $this->handleResponse([
                    "subscriptionExpired" => $res,
                    "expiryDate"=>$expir_date,
                    "serverDate"=> date('d/m/Y h:i:s'),
                ], 'Membership set successfully.');
            }
            elseif ($data[count($response->object()->latest_receipt_info)-1]->product_id=="relief.weeklypayowner") {

                $data_res['result']='Date is Active';
                $this->CreateSubscription(\Auth::user(),$data[count($response->object()->latest_receipt_info)-1]->product_id,$data[count($response->object()->latest_receipt_info)-1]->transaction_id,'0',date('Y-m-d  H:i:s',strtotime($data[count($response->object()->latest_receipt_info)-1]->purchase_date)),date('Y-m-d  H:i:s',strtotime($data[count($response->object()->latest_receipt_info)-1]->expires_date)));
                // $startDate = strtotime(date('Y-m-d h:i:s', strtotime(\Auth::user()->ExpireAt) ) );
                $start_date=strtotime(\Carbon\Carbon::createFromFormat('d/m/Y H:i:s',\Auth::user()->ExpireAt)->format('Y-m-d H:i:s'));

                \Log::info(date('Y-m-d h:i:s'));
                \Log::info(json_encode($start_date));
                $currentDate = strtotime(date('Y-m-d h:i:s'));
                \Log::info(json_encode($currentDate));
                $res=1;
                if($start_date > $currentDate) {
                    $res=0;
                }

                return $this->handleResponse([
                    "subscriptionExpired" => $res,
                    "expiryDate"=>$expir_date,
                    "serverDate"=> date('d/m/Y h:i:s'),
                ], 'Membership set successfully.');
            }
            else {
                $data_res['result']='Date is Expired';
                return $this->handleResponse([
                    "subscriptionExpired" => 0
                ], 'Invalid Product Id.');
            }

        }
    }

    public function setMemberShip(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'orderId' => 'required',
                'productId' => 'required',
                'startDate' => 'required|date_format:d/m/Y H:i:s|'.'after:' . date('Y-m-d'),
                'endDate' => 'required|date_format:d/m/Y H:i:s|after_or_equal:startDate',
            ]);
            if($validator->fails()){
                return $this->handleError(implode(" ", $validator->errors()->all()), null);
            }
                    $user = \Auth::user();
                  if(isset($user))
                  {
                    $Ma=MemberShip::where('user_id',$user->id)->first();
                    if(isset($Ma))
                    {

                    }
                    else
                    {
                        $Ma=new MemberShip();
                    }

                    $start_date=\Carbon\Carbon::createFromFormat('d/m/Y H:i:s',$request->startDate)->format('Y-m-d H:i:s');
                    // date('Y-m-d  H:i:s',strtotime($request->startDate));
                    $Ma->user_id=$user->id;
                    $Ma->start_date=$start_date;
                    $Ma->orderId=$request->orderId;
                    $Ma->productId=$request->productId;
                    $Ma->type="Weekly";
                    $Ma->end_date=\Carbon\Carbon::createFromFormat('d/m/Y H:i:s',$request->endDate)->format('Y-m-d H:i:s');
                    $Ma->save();
                    // $expir_date=date('d/m/Y h:i:s',strtotime());
                    return $this->handleResponse([
                        "expiryDate" =>\Carbon\Carbon::parse($Ma->end_date)->format('d/m/Y h:i:s'),
                        "serverDate"=> date('d/m/Y h:i:s')
                    ],'Membership set successfully.');
                  }
                  else
                  {

                  }
        }
        catch(\exception $e){
	        return $this->handleError($e->getMessage(), null, 401);
	    }

    }

    public function CreateSubscription($user,$orderId,$productId,$Monthly,$start_date,$end_date)
    {
        $Ma=MemberShip::where('user_id',$user->id)->first();
        if(isset($Ma))
        {

        }
        else
        {
            $Ma=new MemberShip();
        }
        $Ma->user_id=$user->id;
        $Ma->start_date=$start_date;
        $Ma->orderId=$orderId;
        $Ma->productId=$productId;
        if($Monthly=='0')
        {
            $Ma->type="Weekly";
        }
        else
        {
            $Ma->type="-";
        }
        $Ma->end_date=$end_date;
        $Ma->save();
    }
}
