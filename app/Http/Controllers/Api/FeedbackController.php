<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Models\Feedback;
use Auth;
use Illuminate\Http\Request;
use Validator;

class FeedbackController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function SendFeedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'feedback' => 'required',
        ]);
        //for validation request

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        //for errors of request

        $feedback = new Feedback();
        $feedback->feedback_string=$request->feedback;
        $feedback->user_id =\Auth::user()->id;
        $feedback->save();

        //store data
        return $this->handleResponse([], __('api.feedback_success'));
    }
}
