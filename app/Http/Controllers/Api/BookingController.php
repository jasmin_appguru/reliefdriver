<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Booking\BookingListDriver;
use App\Http\Resources\Booking\BookingResource;
use App\Http\Resources\Booking\DriverListResources;
use App\Models\Booking;
use App\Models\BookingDate;
use App\Models\Driver;
use App\Models\DriverUnavailableDate;
use App\Models\FlagCheck;
use App\Models\RequestCancel;
use App\Models\User;
use Auth;
use App\Rules\JsonDate;
use Illuminate\Http\Request;
use Validator;

class BookingController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            // 'bookingDate' => 'required|date',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if(\Auth::user()->user_type=="1")
        {
            $Bookings = Booking::with('bookingDates')->Active()->where('driver_id',\Auth::user()->driver->id)->where('is_accept','1')->get();
            $Bookings = $Bookings->sortBy(function($booking){
                return $booking->S_date;
            });
            //$Bookings = Booking::with('bookingDates')->Active()->join('booking_dates', 'booking_dates.booking_id', '=', 'bookings.id')->orderBy('booking_dates.date','asc')
            //->where('bookings.driver_id',\Auth::user()->driver->id)->where('is_accept','1')->get();
        }
        else
        {
            $Bookings = Booking::with('bookingDates')->Active()->where('request_user_id',\Auth::user()->owner->id)->where('is_accept','1')->get();
            $Bookings = $Bookings->sortBy(function($booking){
                return $booking->S_date;
            });
            // $Bookings = Booking::with('bookingDates')->Active()->where('bookings.request_user_id',\Auth::user()->owner->id)->where('is_accept','1')
            // ->join('booking_dates', 'booking_dates.booking_id', '=', 'bookings.id')->orderBy('booking_dates.date','desc')->get();
        }
        return $this->handleResponse(BookingResource::collection($Bookings), 'booking list fetch successfully',200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'colum_01' => 'required',
    //     ]);
    //     //for validation request

    //     if($validator->fails()){
    //         return $this->handleError($validator->errors()->first());
    //     }
    //     //for errors of request

    //     $validatedData = $request->all();
    //     $Booking = Booking::create($validatedData);

    //     //store data
    //     return $this->handleResponse(new \App\Http\Resources\BookingResource($Booking), 'new Booking created.');
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function searchReliefDriver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required',
            'fromDate'  =>  'required|date_format:d/m/Y',
            'toDate'    =>  'required|date_format:d/m/Y',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if($request->fromDate == $request->toDate)
        {

             $Days_list=array();
            $startDate = \Carbon\Carbon::createFromFormat('d/m/Y',$request->fromDate);
            $endDate = \Carbon\Carbon::createFromFormat('d/m/Y',$request->toDate);
            array_push($Days_list,$startDate->format('l'));

            // ->whereHas('bookingDates',function($q){
            //     $q->where('date',">=",date('Y-m-d'));
            // })

            $dates=BookingDate::whereHas('booking',function($q){
                $q->where('is_accept',"=",1);
            })->where('date','=', $startDate->format('Y-m-d'))
            ->groupBy('driver_id')->get(['driver_id']);
            $drivers=array();

            foreach($dates as $d)
            {
                array_push($drivers,$d->driver_id);
            }

            $unavalable_driver=DriverUnavailableDate::where('date','=', $startDate->format('Y-m-d'))
            ->groupBy('driver_id')->get(['driver_id']);
            //whereBetween('date',[$startDate->format('Y-m-d'),$endDate->format('Y-m-d')])

            foreach($unavalable_driver as $d)
            {
                array_push($drivers,$d->driver_id);
            }

            // User::where('username', 'not like', "%ray%")->get();
            //$user=Driver::whereNotIn('availability', $Days_list)->whereHas('user',function($q){

            $user=Driver::where(function($query) use ($Days_list) {
                foreach(array_unique($Days_list) as $day)
                {
                    $query->where('availability', 'LIKE', '%'.$day.'%');
                }
            })
            ->where('hasAvailableDays','true')
            ->whereHas('user',function($q){
                $q->where('is_profile_setup',1);
            })->whereNotIn('id',$drivers)->with('user')->get();

            return $this->handleResponse(DriverListResources::collection($user),__('api.driver_list_booking').json_encode(array_unique($Days_list)));
        }
        else
        {
            \Log::info("Multiple Date");
            \Log::info(json_encode($request->all()));
            $Days_list=array();

            $startDate = \Carbon\Carbon::createFromFormat('d/m/Y',$request->fromDate);
            $endDate = \Carbon\Carbon::createFromFormat('d/m/Y',$request->toDate);

            for ($date = \Carbon\Carbon::createFromFormat('d/m/Y',$request->fromDate); $date <= \Carbon\Carbon::createFromFormat('d/m/Y',$request->toDate); $date->modify('+1 day')) {
                array_push($Days_list,$date->format('l'));
            }
            $dates=BookingDate::whereHas('booking',function($q){
                $q->where('is_accept',"=",'1');
            })
            ->whereBetween('date',[$startDate->format('Y-m-d'),$endDate->format('Y-m-d')])
            // ->where('date','<=', \Carbon\Carbon::createFromFormat('d/m/Y',$request->fromDate)->format('Y-m-d'))
            // ->where('date','>', \Carbon\Carbon::createFromFormat('d/m/Y',$request->toDate)->format('Y-m-d'))
            ->groupBy('driver_id')->get(['driver_id']);
            $drivers=array();

            foreach($dates as $d)
            {
                array_push($drivers,$d->driver_id);
            }

            $unavalable_driver=DriverUnavailableDate::whereBetween('date',[$startDate->format('Y-m-d'),$endDate->format('Y-m-d')])
            //     where('date','<=', $startDate->format('Y-m-d'))
            // ->where('date','>', $endDate->format('Y-m-d'))
            ->groupBy('driver_id')->get(['driver_id']);
            //whereBetween('date',[$startDate->format('Y-m-d'),$endDate->format('Y-m-d')])

            foreach($unavalable_driver as $d)
            {
                array_push($drivers,$d->driver_id);
            }

            // User::where('username', 'not like', "%ray%")->get();
            //$user=Driver::whereNotIn('availability', $Days_list)->whereHas('user',function($q){
            $user=Driver::where(function($query) use ($Days_list) {
                foreach(array_unique($Days_list) as $day)
                {
                    $query->where('availability', 'LIKE', '%'.$day.'%');
                }
            })
            ->where('hasAvailableDays','true')
            ->whereHas('user',function($q){
                $q->where('is_profile_setup',1);
            })->whereNotIn('id',$drivers)->with('user')->get();

            \Log::info(json_encode($request->all()));
            return $this->handleResponse(DriverListResources::collection($user),__('api.driver_list_booking'));
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function filterReliefDriver(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'companyName' => 'array',
        // ]);
        // if($validator->fails()){
        //     return $this->handleError($validator->errors()->first());
        // }

        // $startDate = \Carbon\Carbon::createFromFormat('d/m/Y',$request->fromDate);
        // $endDate = \Carbon\Carbon::createFromFormat('d/m/Y',$request->toDate);

        $user=Driver::whereHas('user',function($q){
            $q->where('is_profile_setup',1);
        })->orderBy('id', 'desc');

        if($request->has('industryExperience') && !empty($request->industryExperience))
        {
            //$user->whereIn('industry_experience',json_decode($request->industryExperience));
            $user->where('industry_experience',$request->industryExperience);
        }
        if($request->has('hourRateStart') && !empty($request->hourRateStart))
        {
            $user->where('day_rate_a',">=",$request->hourRateStart);
        }
        if($request->has('hourRateEnd') && !empty($request->has('hourRateEnd')))
        {
            $user->where('day_rate_a',"<=",$request->hourRateEnd);
        }
        if($request->has('nightRateStart') && !empty($request->nightRateStart))
        {
            $user->where('night_rate_a',">=",$request->nightRateStart);
        }
        if($request->has('nightRateEnd') && !empty($request->has('nightRateEnd')))
        {
            $user->where('night_rate_a',"<=",$request->nightRateEnd);
        }
        if($request->has('companyName'))
        {
            if(!empty($request->companyName))
            {
                foreach($request->companyName as $key => $search)
                {
                    if($key===0)
                    {
                        $user->where('inducted_company','LIKE',"%{$search}%");
                    }
                    else
                    {
                        $user->orWhere('inducted_company','LIKE',"%{$search}%");
                    }

                }
                //$user->whereIn('inducted_company',$request->companyName);
            }
        }
        if($request->has('licenceClass') && !empty($request->has('licenceClass')))
        {
            if($request->licenceClass!="")
            {

                $list=explode(",",$request->licenceClass);
                foreach($list as $chank)
                {
                    $user->where('license_class','LIKE','%'.$chank."%");
                }
            }
        }

        return $this->handleResponse(DriverListResources::collection($user->get()),__('api.driver_list_booking'));
    }

    /**
     * Send A booking Request
     *
     * @param  booking,date,shift,istructio
     * @return \Illuminate\Http\Response
     */
    public function sendBookingRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "truck" => 'required',
            "truckLocation" => 'required',
            "shift" => 'required',
            "latitude" => 'required',
            "longitude" => 'required',
            "reliefDriverId" => 'required',
            "bookingDates" => 'required|array',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        if($request->truck!=\Auth::user()->Owner->truck_number)
        {
            return $this->handleError("Please Enter a valid Truck Number.");
        }
        if($this->CheckDriverArabilityBooking($request->bookingDates,\Auth::user()->Owner->id,$request->shift))
        {
             return $this->handleError(__('api.already_booked'));
        }
        $diver=Driver::find($request->reliefDriverId);
       if($this->CheckDriverArability($request->bookingDates,$request->reliefDriverId))
       {
            return $this->handleError(__('api.driver_not_available_for_the_data',['Driver'=>$diver->user->first_name]));
       }



        $requests_cancel_count=FlagCheck::where('driver_id',$request->reliefDriverId)->where('owner_id',\Auth::user()->owner->id)->where('driver_counter','>',0)->sum('driver_counter');
        if($requests_cancel_count >= 3)
        {
            //"This driver has not accepted your last ".$requests_cancel_count." requests."
            return $this->handleError(__('api.send_booking_driver_warring',['Driver'=>$diver->user->first_name,'count' => $requests_cancel_count]),100);
        }
        //return $this->handleError(\Auth::user()->owner->id,'');

        $Booking = new Booking();
        $Booking->company=\Auth::user()->Owner->business_name ?? "Default Company Name";
        $Booking->truck_no=$request->truck;
        $Booking->latitude=$request->latitude;
        $Booking->longitude=$request->longitude;


        $Booking->truckLocation=$request->truckLocation;
        $Booking->request_user_id=\Auth::user()->owner->id;
        $Booking->driver_id =$request->reliefDriverId;
        $Booking->driver_name=$diver->User->first_name ?? "Driver";
        $Booking->driver_phone=$diver->User->mobile_number;
        $Booking->instruction =$request->instruction ?? "";
        $Booking->shift =$request->shift;
        $Booking->save();

        // <owner driver name> has sent you a booking request for <date>
        SendNotification(__('api.sendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($request->bookingDates)]),$Booking->Driver->User->id,true,"New booking request");

        foreach($request->bookingDates as $date)
        {
            $Booking_dates=new BookingDate();
            $Booking_dates->date=date('Y-m-d',strtotime($date));
            $Booking_dates->shift=$request->shift;
            $Booking_dates->booking_id=$Booking->id;
            $Booking_dates->driver_id=$request->reliefDriverId;
            $Booking_dates->save();
        }


        return $this->handleResponse('',__('api.booking_success',['DRIVER'=>$Booking->driver_name]));
    }

    public function againSendBookingRequest(Request $request)
    {

        $validator = Validator::make($request->all(), [
            "truck" => 'required',
            "truckLocation" => 'required',
            "shift" => 'required',
            "latitude" => 'required',
            "longitude" => 'required',
            "reliefDriverId" => 'required',
            "bookingDates" => 'required|array',
        ]);


        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if($this->CheckDriverArabilityBooking($request->bookingDates,\Auth::user()->Owner->id,$request->shift))
        {
             return $this->handleError(__('api.already_booked'));
        }

        if($request->truck!=\Auth::user()->Owner->truck_number)
        {
            return $this->handleError("Please Enter a valid Truck Number.");
        }
        $diver=Driver::find($request->reliefDriverId);
       if($this->CheckDriverArability($request->bookingDates,$request->reliefDriverId))
       {
            return $this->handleError(__('api.driver_not_available_for_the_data',['Driver'=>$diver->user->first_name]));
       }
        //return $this->handleError(\Auth::user()->owner->id,'');
        $Booking = new Booking();
        $Booking->company=\Auth::user()->Owner->business_name ?? "Default Company Name";
        $Booking->truck_no=$request->truck;
        $Booking->latitude=$request->latitude;
        $Booking->longitude=$request->longitude;


        $Booking->truckLocation=$request->truckLocation;
        $Booking->request_user_id=\Auth::user()->owner->id;
        $Booking->driver_id =$request->reliefDriverId;
        $Booking->driver_name=$diver->User->first_name ?? "Driver";
        $Booking->driver_phone=$diver->User->mobile_number;
        $Booking->instruction =$request->instruction ?? "";
        $Booking->shift =$request->shift;
        $Booking->save();

        // <owner driver name> has sent you a booking request for <date>
        SendNotification(__('api.sendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($request->bookingDates)]),$Booking->Driver->User->id,true,"New booking request");

        foreach($request->bookingDates as $date)
        {
            $Booking_dates=new BookingDate();
            $Booking_dates->date=date('Y-m-d',strtotime($date));
            $Booking_dates->shift=$request->shift;
            $Booking_dates->booking_id=$Booking->id;
            $Booking_dates->driver_id=$request->reliefDriverId;
            $Booking_dates->save();
        }


        return $this->handleResponse('',__('api.booking_success',['DRIVER'=>$Booking->driver_name]));
    }

    public function getSentBookingRequest(Request $request)
    {

        // ->with(['relation2' => function($query)  {
        //     $query->orderBy('relation2_column_name','DESC');
        //    }
        // ])
            $booking=Booking::Active()->where('request_user_id',\Auth::user()->Owner->id)->where('is_accept','0')->get();
            $booking = $booking->sortBy(function($booking){
                return $booking->S_date;
            });
        return $this->handleResponseWithServerDate(BookingListDriver::collection($booking),"Send booking list fetch successfully");
    }

    public function cancelBookings(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "bookingId" => 'required',
            "userId" => 'required'
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $booking=Booking::find($request->bookingId);

        if($booking->is_accept=="1")
        {
            $Cancel=RequestCancel::where('owner_id',\Auth::user()->owner->id)->where('booking_id',$booking->id)->where('driver_id',$booking->driver_id)->first();
            if(empty($Cancel))
            {
                $new_request_counter=new RequestCancel();
                $new_request_counter->driver_id=$booking->driver_id;
                $new_request_counter->booking_id=$booking->id;
                $new_request_counter->owner_counter=1;
                $new_request_counter->counter=0;
                if($request->has("cancelReason"))
                {
                    $new_request_counter->reason=$request->cancelReason;
                }
                $new_request_counter->owner_id=\Auth::user()->owner->id;
                $new_request_counter->save();
            }
            else
            {
                $Cancel->owner_counter=$Cancel->owner_counter+1;
                if($request->has("cancelReason"))
                {
                    $Cancel->reason=$request->cancelReason;
                }
                $Cancel->save();
            }
        }
        if($request->has("cancelReason"))
        {
            SendNotification(__('api.cancelledSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())])." Reason: ".$request->cancelReason,$booking->Driver->User->id,true,"Booking cancelled");
        }
        else
        {
            SendNotification(__('api.cancelledSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())]),$booking->Driver->User->id,true,"Booking cancelled");
        }
        $compompany=$booking->Driver->User->first_name;
        if($booking->is_accept=="1")
        {
            $booking->is_accept=2;
            $booking->save();
            BookingDate::where('date','>=',date('Y-m-d'))->where('booking_id',$booking->id)->update(['is_canceled' => 1]);
            updateFlags($booking);
        }
        else
        {
            BookingDate::where('booking_id',$booking->id)->delete();
            $booking->delete();
        }

        //updateFlage($booking);

        return $this->handleResponse('',__('api.booking_cancelled',["COMPANY"=>$compompany]));

    }

    public function RequestOperation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "bookingId" => 'required',
            "userId" => 'required',
            "isAccept" => 'required'
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $booking= Booking::with('bookingDates')->Active()->where('id',$request->bookingId)->first();
        // dd($booking);

        if($request->isAccept=="1")
        {
            if($booking->is_accept != '0')
            {
                return $this->handleResponse([],__('api.accepted_another'));
            }

            if($this->CheckDriverArability($booking->bookingDates->pluck('date')->toArray(),\Auth::user()->driver->id))
            {
                return $this->handleError("You have already booked another booking. Or you are not available for the same day.");
            }

            // $requests_cancel_count=Booking::where('driver_id',\Auth::user()->driver->id)->where('request_user_id',$booking->request_user_id)->count();
            // $requests_cancel_count=RequestCancel::where('owner_id',$booking->request_user_id)->where('status','complied')->where('driver_id',\Auth::user()->driver->id)->where('owner_counter','>',0)->count();
            // if($requests_cancel_count>=3)
            // {
            //     return $this->handleError($booking->company." has cancelled a booking with you more then 3 times.",100);
            // }
            $requests_cancel_count=FlagCheck::where('owner_id',$booking->request_user_id)->where('driver_id',\Auth::user()->driver->id)->where('owner_counter','>',0)->sum('owner_counter');
            if($requests_cancel_count>=3)
            {
                return $this->handleError($booking->company." has cancelled a booking with you more then 3 times.",100);
            }


            $booking->is_accept="1";
            $booking->save();

            //○ <relief driver name> has accepted your booking request for <date> or <date range>
            // acceptedSendBookingNotification
            SendNotification(__('api.acceptedSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())]),$booking->Owner->User->id,true,"Booking request accepted");
            $Cancel=RequestCancel::where('driver_id',\Auth::user()->driver->id)->where('booking_id',$booking->id)->where('owner_id',$booking->request_user_id)->first();
            if(empty($Cancel))
            {
                $new_request_counter=new RequestCancel();
                $new_request_counter->driver_id=\Auth::user()->driver->id;
                $new_request_counter->booking_id=$booking->id;
                $new_request_counter->owner_id=$booking->request_user_id;
                $new_request_counter->AcceptCounter=1;
                $new_request_counter->save();
            }
            else
            {
                $Cancel->AcceptCounter=$Cancel->AcceptCounter+1;
                $Cancel->save();
            }

            ////////////////////////// CANCAL ANOTHER REQUEST
            cancall_all_same_truck_request($booking,$booking->s_date);

            return $this->handleResponse("",__('api.request_operation_success',['OPERATION' => "accepted",'COMPANY'=>$booking->company]));
        }
        else
        {

            $booking->is_accept="3";
            BookingDate::where('date','>=',date('Y-m-d'))->where('booking_id',$booking->id)->update(['is_canceled' => 1]);
            $booking->save();
            // ○ <relief driver name> has cancelled your booking for <date>
            // ○ <owner driver / business name> has cancelled your booking for <date>
            if($request->has("cancelReason"))
            {
                //SendNotification(__('api.cancelledSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())])."Reason:".$request->cancelReason,$booking->Owner->User->id,true,"Booking cancelled");
                SendNotification(__('api.cancelledSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())])."Reason: ".$request->cancelReason,$booking->Owner->User->id,true,"Booking cancelled");
            }
            else
            {
                SendNotification(__('api.cancelledSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())]),$booking->Owner->User->id,true,"Booking cancelled");
            }

            $Cancel=RequestCancel::where('driver_id',\Auth::user()->driver->id)->where('booking_id',$booking->id)->where('owner_id',$booking->request_user_id)->first();
            if(empty($Cancel))
            {
                $new_request_counter=new RequestCancel();
                $new_request_counter->driver_id=\Auth::user()->driver->id;
                $new_request_counter->booking_id=$booking->id;
                $new_request_counter->owner_id=$booking->request_user_id;
                $new_request_counter->owner_counter=0;
                $new_request_counter->counter=1;
                if($request->has("cancelReason"))
                {
                    $new_request_counter->reason=$request->cancelReason;
                }
                $new_request_counter->save();
            }
            else
            {
                $Cancel->counter=$Cancel->counter+1;
                if($request->has("cancelReason"))
                {
                    $Cancel->reason=$request->cancelReason;
                }
                $Cancel->save();

            }
            // updateFlage($booking);
            updateFlags($booking);
            return $this->handleResponse("",__('api.request_operation_success',['OPERATION' => "declined",'COMPANY'=>$booking->company]));
        }

    }

    public function AgainRequestOperation(Request $request)
    {

        $validator = Validator::make($request->all(), [
            "bookingId" => 'required|exists:bookings,id',
            "userId" => 'required',
            "isAccept" => 'required'
        ]);
        // required_if:list_type,==,selling

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        // $booking= Booking::find($request->bookingId);
        $booking= Booking::with('bookingDates')->Active()->where('id',$request->bookingId)->first();

        if($request->isAccept=="1")
        {
            if($booking->is_accept != '0')
            {
                return $this->handleResponse([],__('api.accepted_another'));
            }

            if($this->CheckDriverArability($booking->bookingDates->pluck('date')->toArray(),\Auth::user()->driver->id))
            {
                return $this->handleError("You have already booked another booking. Or you are not available for the same day.");
            }

            $booking->is_accept="1";
            $booking->save();

            //○ <relief driver name> has accepted your booking request for <date> or <date range>
            //acceptedSendBookingNotification
            $Cancel=RequestCancel::where('driver_id',\Auth::user()->driver->id)->where('booking_id',$booking->id)->where('owner_id',$booking->request_user_id)->first();
            if(empty($Cancel))
            {
                $new_request_counter=new RequestCancel();
                $new_request_counter->driver_id=\Auth::user()->driver->id;
                $new_request_counter->booking_id=$booking->id;
                $new_request_counter->owner_id=$booking->request_user_id;
                $new_request_counter->AcceptCounter=1;
                $new_request_counter->save();
            }
            else
            {
                $Cancel->AcceptCounter=$Cancel->AcceptCounter+1;
                $Cancel->save();
            }

            updateFlage($booking);

            SendNotification(__('api.acceptedSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())]),$booking->Owner->User->id,true,"Booking request accepted");

            ////////////////////////// CANCAL ANOTHER REQUEST
            cancall_all_same_truck_request($booking,$booking->s_date);

            return $this->handleResponse("",__('api.request_operation_success',['OPERATION' => "accepted",'COMPANY'=>$booking->company]));
        }
        else
        {
            $booking->is_accept="3";
            BookingDate::where('date','>=',date('Y-m-d'))->where('booking_id',$booking->id)->update(['is_canceled' => 1]);
            $booking->save();
            // ○ <relief driver name> has cancelled your booking for <date>
            // ○ <owner driver / business name> has cancelled your booking for <date>
            if($request->has("cancelReason"))
            {
                SendNotification(__('api.cancelledSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())])."Reason: ".$request->cancelReason,$booking->Owner->User->id,true,"Booking cancelled");
            }
            else
            {
                SendNotification(__('api.cancelledSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())]),$booking->Owner->User->id,true,"Booking cancelled");
            }
            $Cancel=RequestCancel::where('driver_id',\Auth::user()->driver->id)->where('booking_id',$booking->id)->where('owner_id',$booking->request_user_id)->first();
            if(empty($Cancel))
            {
                $new_request_counter=new RequestCancel();
                $new_request_counter->driver_id=\Auth::user()->driver->id;
                $new_request_counter->booking_id=$booking->id;
                $new_request_counter->owner_id=$booking->request_user_id;
                $new_request_counter->owner_counter=0;
                $new_request_counter->counter=1;
                if($request->has("cancelReason"))
                {
                    $new_request_counter->reason=$request->cancelReason;
                }
                $new_request_counter->save();
            }
            else
            {
                $Cancel->counter=$Cancel->counter+1;
                if($request->has("cancelReason"))
                {
                    $Cancel->reason=$request->cancelReason;
                }
                $Cancel->save();
            }
            //updateFlage($booking);
            updateFlags($booking);

            return $this->handleResponse("",__('api.request_operation_success',['OPERATION' => "declined",'COMPANY'=>$booking->company]));
        }

    }

    public function getBookingRequestList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "userId" => 'required',
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $booking=Booking::with('bookingDates')->Active()->where('driver_id',\Auth::user()->driver->id)->where('is_accept','0')->get();
        $booking = $booking->sortBy(function($booking){
            return $booking->S_date;
        });

        return $this->handleResponseWithServerDate(BookingListDriver::collection($booking),__('api.get_booking_request_success',['OPERATION' => "declined"]));
    }

    public function CheckDriverArability($Dates,$driverId)
    {
        $Driver=Driver::find($driverId);
        if($Driver->hasAvailableDays == "false")
        {
            \Log::debug("False");
            return true;
        }
            foreach($Dates as $date)
            {
                $day=date('l',strtotime($date));
                // $day = \Carbon\Carbon::createFromFormat('d M Y',$date)->format('l');
                $driver_day=Driver::where('id',$driverId)->where(function($query) use ($day) {
                        $query->where('availability', 'LIKE', '%'.$day.'%');
                })->first();


                if(empty($driver_day))
                {
                    return true;
                }

                $dateResulr=DriverUnavailableDate::where('date',date('Y-m-d',strtotime($date)))->where('driver_id',$driverId)->first();
                if(!empty($dateResulr))
                {
                    return true;
                }
                // is_accept
                $bookingResult=Booking::where('is_accept',1)->where('driver_id',$driverId)->whereHas('bookingDates',function($q) use ($date){
                    $q->where('date',date('Y-m-d',strtotime($date)));
                })->first();

                if(!empty($bookingResult))
                {
                    return true;
                }
            }

        // $users = User::whereHas('posts', function($q){
        //     $q->where('created_at', '>=', '2015-01-01 00:00:00');
        // })->get();

        return false;
    }

    public function CheckDriverArabilityBooking($Dates,$owner_id,$shift)
    {
            foreach($Dates as $date)
            {
                $date=date('Y-m-d',strtotime($date));
                $bookingResult=BookingDate::where('date',$date)->whereHas('Booking',function($q) use ($owner_id,$shift){
                    $q->where('request_user_id',$owner_id);
                    $q->where('shift',$shift);
                    $q->where('is_accept','1');
                })->first();
                if(!empty($bookingResult))
                {
                    return true;
                }
            }

        // $users = User::whereHas('posts', function($q){
        //     $q->where('created_at', '>=', '2015-01-01 00:00:00');
        // })->get();

        return false;
    }

}
