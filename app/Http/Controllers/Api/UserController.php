<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Resources\Driver\UnAvailableDatesResource;
use App\Http\Resources\DriverResource;
use App\Http\Resources\UserResource;
use App\Models\ApplicationNotification;
use App\Models\Booking;
use App\Models\Driver;
use App\Models\DriverUnavailableDate;
use App\Models\MemberShip;
use App\Models\OtpManager;
use App\Models\Owner;
use App\Models\RequestCancel;
use Validator;
use Auth;
use Illuminate\Support\Facades\Hash;
use Facade\FlareClient\Api;
use Exception;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Password;

class UserController extends BaseController
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8|max:16',
            'mobileNumber' => 'required',
            'otp' => 'required',
            'deviceType' => 'required',
            'userType' => 'required',
            'deviceToken' => 'required',
        ],[
            'email.unique' => __('api.register_email_unique')
        ]);

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        if($user = User::where('email',$request->email)->where('user_type',$request->userType)->first())
        {
            return $this->handleError(__('api.register_email_unique'));
        }
        if($user = User::where('mobile_number',$request->mobileNumber)->where('user_type',$request->userType)->first())
        {
            return $this->handleError(__('api.register_mobile_number_unique'));
        }
        $validatedData = $request->all();
        $validatedData['mobile_number']=$request->mobileNumber;
        $validatedData['user_type']=$request->userType;
        $validatedData['device_type']=$request->deviceType;
        $validatedData['device_token']=$request->deviceToken;
        $validatedData['otp']=$request->otp;
        $validatedData['password'] = Hash::make($request->password);
        if($request->has('otp'))
        {
            if(!$this->_validateOTP($request->mobileNumber,$request->otp))
            {
                $user = User::create($validatedData);
                if($user->user_type==1)
                {
                    $driver=new Driver();
                    $driver->user_id=$user->id;
                    $driver->save();
                }
                else
                {
                    $owner=new Owner();
                    $owner->user_id=$user->id;
                    $owner->save();
                }
                $accessToken = $user->createToken('authToken')->accessToken;
                $user->tokan=$accessToken;
            }
            else
            {
                return $this->handleError(__('api.invalid_otp'));
            }
        }
        return $this->handleResponse(new UserResource($user), __('api.signup'));
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'deviceToken' => 'required',
            'deviceType' => 'required',
            'userType' => 'required',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password,'user_type' => $request->userType])){
            $auth = Auth::user();

            // $success['token'] =  $auth->createToken('authToken')->accessToken;
            if($request->has('deviceToken'))
            {
                $auth->device_token=$request->deviceToken;
                $auth->device_type=$request->deviceType;
                $auth->save();
            }

            $request->user()->tokens->each(function($token, $key) {
                $token->delete();
            });

            $tokan=$auth->createToken('authToken')->accessToken;
            $auth->tokan=$tokan;


//             $user=User::with('Membership')->find($auth->id);
// dd($user);
            return $this->handleResponse(new UserResource($auth), __('api.login_success'));
        }
        else{
            return $this->handleError(__('api.invalid'));
        }
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'currentPassword' => 'required',
            'newPassword' => 'required|min:8|max:16',
            'userId' => 'required',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $user=\Auth::user();
        $user->password=Hash::make($request->newPassword);
        $user->save();

        return $this->handleResponse([],__('api.change_password'));
    }

    public function getVerificationCode(Request $request)
    {
        $role_j=array(
            'mobileNumber' => 'required'
        );

        $validator = Validator::make($request->all(),$role_j);
        if($validator->fails()){
            return $this->handleError($validator->errors());
        }
        $sentotp=$this->_SendOtp($request->mobileNumber,(string)rand(1111,9999));
        if($sentotp === true)
        {
            return $this->handleResponse([],__('api.getVerificationCode'));
        }
        return $this->handleError(__('api.error_otp_api'),$sentotp);
    }

    public function _SendOtp($receiverNumber,$otp)
    {
       // $otp="1234";
        $message =__('api.otp_massage',['OTP' => $otp,'purpose'=>'sign up']);

        try {
            if(config('relief_driver.TWILIO_ENABLE'))
            {
                $account_sid = config('relief_driver.TWILIO_SID');
                $auth_token = config('relief_driver.TWILIO_TOKEN');
                $twilio_number = config('relief_driver.TWILIO_FROM');
                $client = new Client($account_sid,$auth_token);
                $client->messages->create($receiverNumber, [
                    'from' => $twilio_number,
                    'body' => $message]);
            }else{
                $otp="1234";
            }
            OtpManager::updateOrCreate(['mobile_number'=>$receiverNumber],['otp'=>$otp]);
            return true;
        } catch (Exception $e) {
            \Log::alert($e->getMessage());
            return false;
        }
    }

    public function _validateOTP($receiverNumber,$otp)
    {
        $otp_manager=OtpManager::where('mobile_number',$receiverNumber)->where('otp',$otp)->orderBy('id','DESC')->first();
        if($otp_manager)
        {
            $otp_manager->otp="";
            $otp_manager->save();
            return 0;
        }
        else
        {
            return 1;
        }
    }

    public function Logout(Request $request)
    {
        $u = Auth::user();
        $u->device_token="";
        $u->save();
        $user = Auth::user()->token();
        $user->revoke();

        return $this->handleResponse([],"Logout Successfully");
    }

    public function ForgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'userType' => 'required',
        ]);
        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }

        $user=User::where('email',$request->email)->where('user_type',$request->userType)->first();
        if(!$user)
        {
            return $this->handleError("The email has not been registered with this user.");
        }
        $input = $request->only('email');

        $response = Password::sendResetLink([
            'email' => $request->only('email'),
            'user_type' => $request->only('userType'),
        ]);

        $message = $response === Password::RESET_LINK_SENT ? $response : __($response);

        return $this->handleResponse([],__($message));
    }

    public function loginUser(Request $request)
    {
        return \Auth::user();
    }

    public function deleteUser(Request $request)
    {
        $user=\Auth::user();
        if($user->user_type=='1')
        {
            \Log::info("::::::::::::::::Delete Driver User Start ::::::::::::::::");
            $driver=Driver::where('user_id',\Auth::user()->id)->first();
            $bookings=Booking::where('driver_id',$driver->id)->where('is_accept','1')->get();
            foreach($bookings as $booking)
            {
                \Log::info("::::::::::::::::Delete Booking ::::::::::::::::");
                SendNotification(__('api.cancelledSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())]),$booking->Driver->User->id,true,"Booking cancelled");
                $bookings->delete();
            }
            $deleteRecode=Booking::where('driver_id',$driver->id)->get();
            foreach($deleteRecode as $deleteSelect)
            {
                $deleteSelect->bookingDates()->delete();
                $deleteSelect->delete();
            }

            $unavilable=DriverUnavailableDate::where('driver_id',$driver->id)->get();
            foreach($unavilable as $und)
            {
                $und->delete();
            }
            $Member=MemberShip::where('user_id',\Auth::user()->id)->first();
            if($Member)
            {
                $Member->delete();
            }
            \Log::info("::::::::::::::::Delete User:::::::::::::::::". \Auth::user()->id);
            $dataNotification=ApplicationNotification::where('user_id',\Auth::user()->id)->get();
            foreach($dataNotification as $notification)
            {
                $notification->delete();
            }

            $requestcn=RequestCancel::where('driver_id',$driver->id)->get();
            foreach($requestcn as $re)
            {
                \Log::info("::::::::::::::::Member Calculation ::::::::::::::::");
                $re->delete();
            }
            \Log::info("::::::::::::::::Driver Id ::::::::::::::::".$driver->id);
            Driver::find($driver->id)->delete();
            \Log::info("::::::::::::::::Delete User Driver :::::::::::::::::". \Auth::user()->id);
            User::find(\Auth::user()->id)->delete();

            return $this->handleResponse("","Your account has been deleted.");

        }
        else
        {
            \Log::info("::::::::::::::::Delete Owner User Start ::::::::::::::::");
            $owner=Owner::where('user_id',\Auth::user()->id)->first();
            $bookings=Booking::where('request_user_id',$owner->id)->where('is_accept','1')->get();
            //dd($bookings);
            foreach($bookings as $booking)
            {
                \Log::info("::::::::::::::::Delete Booking ::::::::::::::::");
                SendNotification(__('api.cancelledSendBookingNotification',["DRIVER"=>\Auth::user()->full_name,"DATES"=>date_formate_notification($booking->bookingDates->pluck('date')->toArray())]),$booking->Driver->User->id,true,"Booking cancelled");
                $booking->bookingDates()->delete();
                $booking->delete();
            }
            $deleteRecode=Booking::where('request_user_id',$owner->id)->get();
            foreach($deleteRecode as $deleteSelect)
            {
                \Log::info("::::::::::::::::Delete Booking Request Count::::::::::::::::");
                $deleteSelect->bookingDates()->delete();
                $deleteSelect->delete();
            }
            $Member=MemberShip::where('user_id',\Auth::user()->id)->first();
            if($Member)
            {
                \Log::info("::::::::::::::::Member Ship::::::::::::::::");
                $Member->delete();
            }
            $requestcn=RequestCancel::where('owner_id',$owner->id)->get();
            foreach($requestcn as $re)
            {
                \Log::info("::::::::::::::::Member Calculation ::::::::::::::::");
                $re->delete();
            }
            // request_cancels
            \Log::info("::::::::::::::::Owner Id ::::::::::::::::".$owner->id);
            Owner::find($owner->id)->delete();
            \Log::info("::::::::::::::::Delete User Owner :::::::::::::::::". \Auth::user()->id);
            User::find(\Auth::user()->id)->delete();

            return $this->handleResponse("","Your account has been deleted.");
        }
        //7.83
    }

    public function unauthorize(Request $request)
    {
        return $this->handleResponse([],'Invalid Url');
    }

    public function WelcomEmail()
    {
        dd(\Auth::user()->WelcomEmail());
        dd("sent welcome mail");
    }
}
