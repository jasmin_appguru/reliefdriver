<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Models\Faq;
use Auth;
use Illuminate\Http\Request;
use Validator;

class FaqController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Faqs = Faq::all();
        return $this->handleResponse(\App\Http\Resources\FaqResource::collection($Faqs),__('api.faq_index'),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'colum_01' => 'required',
        ]);
        //for validation request

        if($validator->fails()){
            return $this->handleError($validator->errors()->first());
        }
        //for errors of request

        $validatedData = $request->all();
        $Faq = Faq::create($validatedData);

        //store data
        return $this->handleResponse(new \App\Http\Resources\FaqResource($Faq), 'new Faq created.');
    }

}
