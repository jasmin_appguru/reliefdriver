<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Str;

class FeedbackController extends Controller
{
    public function getalldata()
    {
        $users = Feedback::orderBy('id', 'DESC')->get();
        return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('name', function($users){
                    $fullName = $users->User->first_name.' '.$users->User->last_name;
                    $profile = '<a class="avatar" href="javascript:void(0)"> <img alt="" class="img-fluid" src="'.$users->User->image.'"></a>';
                    return $fullName;
                })
                ->editColumn('feedback_text', function($users){
                    return Str::limit($users->feedback_string, 100, '...');
                })
                ->editColumn('email', function($users){
                    $email = isset($users->User->email)?trim($users->User->email):"N/A";
                    return $email;
                })
                ->editColumn('mobile', function($users){
                    $mobile = isset($users->User->mobile_number) ? trim($users->User->mobile_number) : "N/A";
                    return $mobile;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/Feedback/".$user->id);
                    },
                ])
                ->rawColumns(['name','status'])
                ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = "Feedback List";
        $users = Feedback::orderBy('id', 'DESC')->get();
        return view('admin.Feedback.index',compact('pageTitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function show($feedback)
    {
        $page = "Feedback Details";
        $user = Feedback::where('id',$feedback)->first();
        if($user){
            return view('admin.Feedback.view', compact('user','page'));
        }
        else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedback $feedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $feedback)
    {
        //
    }
}
