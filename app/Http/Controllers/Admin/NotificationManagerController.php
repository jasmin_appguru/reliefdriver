<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ApplicationNotification;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Routing\Route;

class NotificationManagerController extends Controller
{
    public function getalldata()
    {
        $users = ApplicationNotification::orderBy('id', 'DESC')->get();
        return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('name', function($users){
                    $fullName = $users->User->first_name.' '.$users->User->last_name;
                    return $fullName;
                })
                ->addColumn('status', function($users){
                    $is_active = ($users->is_read == 0) ? '<span class="badge badge-success">Read</span>' : '<span class="badge badge-danger">Un Read</span>';
                    return $is_active;
                })
                ->addColumn('type', function($users){
                    $is_active = ($users->user->user_type == 1) ? '<span class="badge badge-info">Driver</span>' : '<span class="badge badge-info">Owner</span>';
                    return $is_active;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/Feedback/".$user->id);
                    },
                ])
                ->rawColumns(['name','status','type'])
                ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.NotificationManager.index')->with([
            'page' => "Notification Manager",
            'ajaxUrl' => route('Ajax.Notification'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ApplicationNotification  $applicationNotification
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationNotification $applicationNotification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ApplicationNotification  $applicationNotification
     * @return \Illuminate\Http\Response
     */
    public function edit(ApplicationNotification $applicationNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ApplicationNotification  $applicationNotification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApplicationNotification $applicationNotification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ApplicationNotification  $applicationNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationNotification $applicationNotification)
    {
        //
    }
}
