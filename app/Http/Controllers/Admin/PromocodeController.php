<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Affiliation;
use App\Models\Country;
use App\Models\City;
use Redirect;
use Yajra\Datatables\Datatables;
use File;
use Image;

class PromocodeController extends Controller
{


    public function index()
    {
        //echo "sadfasdfds";die;
        $pageTitle = "Agent List";
        $users = Affiliation::orderBy('id', 'DESC')->get();
        //print_r($users);die;
        return view('admin.affiliationManagement.index', compact('users', 'pageTitle'));
    }


    public function getAllData()
    {

        if(isset($_REQUEST['order'])){
            $users = Affiliation::get();
        }
        else{
            $users = Affiliation::orderBy('id', 'DESC')->get();
        }

       // echo "<pre>";print_r($users);die;

        return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('name', function($users){
                    //$fullName = $users->first_name.' '.$users->last_name;
                    //echo $fullName;die;
                    $profileUrl = ($users->affiliate_image) ? url('uploads/affiliate_image/'.$users->affiliate_image) : url('uploads/noimg.png');
                    $profile = '<img alt="" class="img-fluid avatar" src="'.$profileUrl.'">';

                    return $profile;

                })
                ->editColumn('email', function($users){
                    $affilate_link = isset($users->affilate_link)?trim($users->affilate_link):"N/A";
                    $affilate_link = '<a target="_blank" href="'.$users->affilate_link.'">'.$users->affilate_link.'</a>';
                    return $affilate_link;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/affiliation-management/".$user->id);
                    },
                ])
                ->rawColumns(['name','email'])
                ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        ini_set("memory_limit", "100M");
        ini_set('post_max_size', '50M');
        ini_set('upload_max_filesize', '50M');

        $aff=new Affiliation();

            $aff->affiliate_image=$request->affimage;
            $aff->affilate_link=$request->afflink;
           // $aff->created_datetime=date('Y-m-d H:i:s');

            $image = $request->file('affimage');
            $imageMainPath = './uploads/affiliate_image/';
            $aff_image = time().'-aff.'.$image->getClientOriginalExtension();
            $image->move($imageMainPath, $aff_image);

            $aff->affiliate_image = $aff_image;

            $aff->save();
            return redirect()->back()->with("success","Affiliate has been added successfully.");
    }

    public function show($id)
    {
        $page = "User Details";

        $user = Affiliation::where('id',$id)->first();
        // $countries = Country::all();
        // $citys = City::where('country_id',$user->country)->orderBy('id','ASC')->get();
        if($user){
            return view('admin.affiliationManagement.view', compact('user','page'));
        }
        else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        ini_set("memory_limit", "100M");
        ini_set('post_max_size', '50M');
        ini_set('upload_max_filesize', '50M');

        $user = Affiliation::find($id);

        $page = "Affiliate Details";
        if($user){
            $updateData = [
                "affilate_link" => $request->afflink
            ];

            if($request->affimage){

                $image = $request->file('affimage');
                $imageMainPath = './uploads/affiliate_image/';
                $aff_image = time().'-aff.'.$image->getClientOriginalExtension();
                $image->move($imageMainPath, $aff_image);

                $updateData['affiliate_image'] = $aff_image;
            }

           // echo "<pre>";print_r($updateData);die;
            Affiliation::where("id",$id)->update($updateData);

            return Redirect::to("admin/affiliation-management")->with("success","Affiliation details has been updated successfully.");
        }
        else{
            return Redirect::to("admin/affiliation-management")->with("error","Something went wrong. Please try again!!");
        }
    }


    public function update(Request $request, $id)
    {
        $user = Affiliation::find($id);

        $page = "Affiliate Details";
        if($user){
            $updateData = [
                "affilate_link" => $request->afflink
            ];

            if($request->affimage){
                $imageMainPath = '/uploads/affiliate_image/';
                $imageThumbPath = $imageMainPath.'/thumbnail/';
                if (!is_dir(public_path($imageMainPath))) {
                    mkdir(public_path($imageMainPath), 777, true);
                }
                if (!is_dir(public_path($imageThumbPath))) {
                    mkdir(public_path($imageThumbPath), 777, true);
                }

                $profile_image = "aff-".time().".png";
                $path = public_path($imageMainPath.$profile_image);

                $image = $request->affiliate_image;  // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                \File::put($path, base64_decode($image));

                $updateData['affiliate_image'] = $profile_image;
            }

            Affiliation::where("id",$id)->update($updateData);

            return Redirect::to("admin/affiliation-management")->with("success","Agent details has been updated successfully.");
        }
        else{
            return Redirect::to("admin/affiliation-management")->with("error","Something went wrong. Please try again!!");
        }
    }


    public function destroy($id)
    {
        $userDB = Affiliation::find($id);

        if($userDB)
        {
            $userDB->delete();

            return redirect()->back()->with("success","Affiliation has been deleted successfully.");
        }
        else
        {
            return redirect()->back()->with("error","Opps!! Something went wrong. Please try again.");
        }
    }



    public function exportUser()
    {
      return Excel::download(new AllAgentsExport, 'AllAgents-'.date('d-m-Y').'.csv');

    }
    public function inactiveUser($id)
    {
        $updateData = [ 'is_active' => 1, ];
        Affiliation::where("id",$id)->update($updateData);
        return Redirect::back()->with("success","Agent has been in-activated successfully.");

    }
    public function activeUser($id)
    {
        $updateData = [ 'is_active' => 0, ];
        Affiliation::where("id",$id)->update($updateData);
        return Redirect::back()->with("success","Agent activated successfully.");

    }


}
