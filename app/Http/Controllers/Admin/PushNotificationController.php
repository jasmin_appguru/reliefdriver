<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PushNotification;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class PushNotificationController extends Controller
{
    public function getalldata()
    {
        $users = PushNotification::orderBy('id', 'DESC')->get();
        return Datatables::of($users)
                ->addIndexColumn()
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/Feedback/".$user->id);
                    },
                ])
                ->rawColumns(['name','status','type'])
                ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.NotificationManager.index')->with([
            'page' => "Push Notification Manager",
            'ajaxUrl' => route('Ajax.Notification'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required|max:150',
            'type' => 'required'
        ]);

        $pushNotification=new PushNotification();
        $pushNotification->description=$request->description;
        $pushNotification->type=$request->type;
        $pushNotification->save();

        if($request->type=="Driver")
        {
            $data=User::where('user_type','1')->where('device_token',"!=",NULL)->get();
        }
        elseif($request->type=="Owner")
        {
            $data=User::where('user_type','2')->where('device_token',"!=",NULL)->get();
        }
        else
        {
            $data=User::where('device_token',"!=",NULL)->get();
        }

        $push_token=array();
        $push_token_ios=array();

        foreach($data as $user)
        {
            if(!empty($user->device_token) && $user->device_type=="1")
            {
                array_push($push_token_ios,$user->device_token);
            }
            elseif(!empty($user->device_token) && $user->device_type=="2")
            {
                array_push($push_token,$user->device_token);
            }
            else
            {
            }
            SendNotification($request->description,$user->id,"false");
        }

        if(count($push_token)>0)
        {
            send_notification_android($push_token,["title"=>__('api.push_notification_title'),"body"=>$pushNotification->description],true);
        }

        if(count($push_token_ios)>0)
        {
            send_notification_ios($push_token_ios,["title"=>__('api.push_notification_title'),"body"=>$pushNotification->description],true);
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PushNotification  $pushNotification
     * @return \Illuminate\Http\Response
     */
    public function show(PushNotification $pushNotification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PushNotification  $pushNotification
     * @return \Illuminate\Http\Response
     */
    public function edit(PushNotification $pushNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PushNotification  $pushNotification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PushNotification $pushNotification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PushNotification  $pushNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(PushNotification $pushNotification)
    {
        //
    }
}
