<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\FlagCheck;
use App\Models\RequestCancel;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Builder;

class BookingController extends Controller
{
    public function getalldata()
    {
        if(isset($_REQUEST['order'])){
            $booking = Booking::where('is_accept','1')->get();
        }
        else{
            $booking = Booking::where('is_accept','1')->orderBy('id', 'DESC')->get();
        }

        return Datatables::of($booking)
                ->addIndexColumn()
                ->editColumn('name', function($booking){
                    $fullName = $booking->Owner->User->first_name.' '.$booking->Owner->User->last_name;
                    return $fullName;
                })
                ->editColumn('driver', function($booking){
                    return $booking->Driver->User->first_name.' '.$booking->Driver->User->last_name;
                })
                ->editColumn('truck_no', function($users){
                    return $users->truck_no;
                })
                ->editColumn('truckLocation', function($users){
                    return $users->truckLocation;
                })
                ->addColumn('shift', function($users){
                    return  $users->shift;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/Booking/".$user->id);
                    },
                ])
                ->rawColumns(['name','status'])
                ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = "Owner List";
        $users = Booking::orderBy('id', 'DESC')->get();
        // dd($users);
        return view('admin.Booking.index', compact('users', 'pageTitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show($booking)
    {
        $page = "User Booking ";
        $user = Booking::where('id',$booking)->first();
        if($user){
            return view('admin.Booking.view', compact('user','page'));
        }
        else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(booking $booking)
    {
        //
    }

    public function CountCounter()
    {
        $booking=Booking::with('bookingDates','Details')->whereHas('bookingDates',function($q){
            $q->where('date',"<",date('Y-m-d'));
        })->where('status','!=','complied')->where('is_accept','!=','0')->orderBy('id','ASC')->get();

        foreach($booking as $book)
        {
            $book->status="complied";
            print_r($book->id."BookingId");
            echo "<br>";
            print_r(json_encode($book));
            echo "<br>";
            echo "<br>";
            print_r(json_encode($book->Details));
            $data_res=updateFlags($book);
            echo "<br>";
            $book->save();
            $book->Details()->update(['status' => 'complied']);
        }

        //dd($booking);
    }

}
