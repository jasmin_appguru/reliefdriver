<?php
/**
    * Author Name: LS
    * Datetime: 2021-08-16
    * Dashboard Controller class for dashboard 'Admin' section
**/

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Driver;
use App\Models\Owner;
use App\Helper\Helper;
use App\Models\Booking;
use App\Models\BookingDate;
use App\Models\Feedback;
use App\Models\User;
use Redirect;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Image;
use Storage;
use Config;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
	protected $breadcrumb = "Dashboard";
    protected $pageTitle = "Dashboard";

    public function index(Request $request){
    	$breadcrumb = $this->breadcrumb;
        $pageTitle = $this->pageTitle;

        $Owner = User::where('is_profile_setup',1)->where('user_type',2)->count();
        $Driver = User::where('is_profile_setup',1)->where('user_type',1)->count();
        $feedback = Feedback::count();
        $booking = BookingDate::where('date', '>=',date('Y-m-d'))->whereHas('Booking', function($q){ $q->where('is_accept','1'); })->count();
        $NextBooking = BookingDate::where('date', '<',date('Y-m-d'))->whereHas('Booking', function($q){ $q->where('is_accept','1'); })->count();
        // $booking = Booking::where('is_accept','1')->whereHas('bookingDates', function($q){ $q->where('date', '>=',date('Y-d-m')); }) ->count();
        // $NextBooking = Booking::where('is_accept','1')->whereHas('bookingDates', function($q){ $q->where('date', '<',date('Y-d-m')); }) ->count();
        $bookingAmount = Booking::past()->where('is_accept','1')->get()->pluck('BookingAmount')->sum();
        // dd(BookingDate::where('date', '=',date('Y-m-d'))->pluck('booking_id')->toArray());
        $bookingCurrent =  Booking::whereIn('id',BookingDate::where('date', '>=',date('Y-m-d'))->whereHas('Booking', function($q){ $q->where('is_accept','1'); })->pluck('booking_id')->toArray())->get()->pluck('BookingAmount')->sum();
        // Booking::whereIn('is_accept',[2,3])->get()->pluck('CancelAmount')->sum();
        $bookingCNAmount = Booking::whereIn('is_accept',[2,3])->get()->pluck('CancelAmount')->sum();
        $start_date=$range=$end_date=$to_date="";

        return view("admin.dashboard",compact('Driver','Owner','booking','feedback','bookingAmount','bookingCNAmount','pageTitle','range','to_date','end_date','start_date','NextBooking','bookingCurrent'));
    }

    public function filter(Request $request)
    {
        $request->validate([
            'range' => 'required'
        ]);

        $Owner = User::query();
        $Owner->where('is_profile_setup',1)->where('user_type',2);
        $Driver = User::query();
        $Driver->where('is_profile_setup',1)->where('user_type',1);
        //$booking = Booking::where('is_accept','1');
        $booking = BookingDate::where('date', '>=',date('Y-m-d'))->whereHas('Booking', function($q){ $q->where('is_accept','1'); });

        $feedback = Feedback::query();
        $bookingAmount = Booking::past()->where('is_accept','1');
        $bookingCNAmount = Booking::whereIn('is_accept',[2,3]);
        $NextBooking = BookingDate::where('date', '<',date('Y-m-d'))->whereHas('Booking', function($q){ $q->where('is_accept','1'); })->count();
        $bookingCurrent =  Booking::whereIn('id',BookingDate::where('date', '>=',date('Y-m-d'))->whereHas('Booking', function($q){ $q->where('is_accept','1'); })->pluck('booking_id')->toArray())->get()->pluck('BookingAmount')->sum();

        if($request->range=="daily")
        {
            $Owner->whereDay('created_at', '=', date('d'));
            $Driver->whereDay('created_at', '=', date('d'));
            // $booking->where('date', '>', date('Y-m-d'));
            $feedback->whereDay('created_at', '=', date('d'));
            $bookingAmount->whereHas('bookingDates',function($q) {
                $q->where('date',"=",date('Y-m-d'));
            });
            $bookingCNAmount->whereHas('bookingDates',function($q) {
                $q->where('date',"=",date('Y-m-d'));
            })->whereIn('is_accept',[2,3]);
            //$bookingCNAmount->whereDay('created_at', '=', date('d'))->get()->pluck('CancelAmount')->sum();
        }elseif($request->range=="weekly")
        {
            $Owner->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            $Driver->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            //$booking->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            $feedback->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            $bookingAmount->whereHas('bookingDates',function($q) {
                $q->whereBetween('date',[Carbon::now()->startOfWeek()->format('Y-m-d'), Carbon::now()->endOfWeek()->format('Y-m-d')]);
            })->where('is_accept','1');
            $bookingCNAmount->whereHas('bookingDates',function($q) {
                $q->whereBetween('date',[Carbon::now()->startOfWeek()->format('Y-m-d'), Carbon::now()->endOfWeek()->format('Y-m-d')]);
            })->whereIn('is_accept',[2,3]);
            // $bookingAmount->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            // $bookingCNAmount->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
        }elseif($request->range=="monthly")
        {
            $Owner->whereMonth('created_at', '=', date('m'));
            $Driver->whereMonth('created_at', '=', date('m'));
            //$booking->whereMonth('created_at', '=', date('m'));
            $feedback->whereMonth('created_at', '=', date('m'));
            $bookingAmount->whereHas('bookingDates',function($q) {
                $q->whereMonth('date', '=', date('m'));
            })->where('is_accept','1');
            $bookingCNAmount->whereHas('bookingDates',function($q) {
                $q->whereMonth('date', '=', date('m'));
            })->whereIn('is_accept',[2,3]);
        }elseif($request->range=="quarterly")
        {
            $date = new \Carbon\Carbon('-3 months');
            $Owner->whereBetween('created_at', [$date->startOfQuarter(), now()]);
            $Driver->whereBetween('created_at', [$date->startOfQuarter(), now()]);
            //$booking->whereBetween('created_at', [$date->startOfQuarter(), now()]);
            $feedback->whereBetween('created_at', [$date->startOfQuarter(), now()]);
            $bookingAmount->whereHas('bookingDates',function($q) use($date) {
                $q->whereBetween('date',  [$date->startOfQuarter(), now()]);
            })->where('is_accept','1');
            $bookingCNAmount->whereHas('bookingDates',function($q) use($date) {
                $q->whereBetween('date',  [$date->startOfQuarter(), now()]);
            })->whereIn('is_accept',[2,3]);
        }elseif($request->range=="yearly")
        {
            $Owner->whereYear('created_at', '=', date('Y'));
            $Driver->whereYear('created_at', '=', date('Y'));
            //$booking->whereYear('created_at', '=', date('Y'));
            $feedback->whereYear('created_at', '=', date('Y'));
            $bookingAmount->whereHas('bookingDates',function($q) {
                $q->whereYear('date',  date('Y'));
            })->where('is_accept','1');
            $bookingCNAmount->whereHas('bookingDates',function($q) {
                $q->whereYear('date',  date('Y'));
            })->whereIn('is_accept',[2,3]);
        }elseif($request->range=="custom")
        {
            $Owner->whereBetween('created_at', [$request->start_date, $request->end_date]);
            $Driver->whereBetween('created_at', [$request->start_date, $request->end_date]);
            //$booking->whereBetween('created_at', [$request->start_date, $request->end_date]);
            $feedback->whereBetween('created_at', [$request->start_date, $request->end_date]);
            $bookingAmount->whereHas('bookingDates',function($q) use($request) {
                $q->whereBetween('date', [$request->start_date, $request->end_date]);
            });
            $bookingCNAmount->whereHas('bookingDates',function($q) use($request){
                $q->whereBetween('date', [$request->start_date, $request->end_date]);
            })->whereIn('is_accept',[2,3]);
        }elseif($request->range=="todate" && $request->to_date!="")
        {
            $Owner->where('created_at', '<=', $request->to_date);
            $Driver->where('created_at', '<=', $request->to_date);
            //$booking->where('created_at', '<=', $request->to_date);
            $feedback->where('created_at', '<=', $request->to_date);
            $bookingAmount->whereHas('bookingDates',function($q) use($request) {
                $q->where('date', '<=', $request->to_date);
            })->where('is_accept','1');
            $bookingCNAmount->whereHas('bookingDates',function($q) use($request){
                $q->where('date', '<=', $request->to_date);
            })->whereIn('is_accept',[2,3]);
        }else
        {
            // $booking->whereDay('created_at', '>', date('d'));
            //$bookingAmount->get()->pluck('BookingAmount')->sum();
            //$bookingCNAmount->get()->pluck('CancelAmount')->sum();
        }

        // dd($bookingAmount->whereHas('bookingDates',function($q) {
        //     $q->where('date',"=",date('Y-m-d'));
        // })->count());

        // dd(Booking::find(13)->amount * $bookingAmount->whereHas('bookingDates',function($q) {
        //     $q->where('date',"=",date('Y-m-d'));
        // })->count());
        // dd($bookingAmount->get()->pluck('BookingAmount'));
        return view("admin.dashboard")->with([
            'range' => $request->range,
            'start_date' => $request->start_date ?? "",
            'end_date' => $request->end_date ?? "",
            'to_date' => $request->to_date ?? "",
            'Owner' => $Owner->count(),
            'Driver' => $Driver->count(),
            'booking' => $booking->count(),
            'feedback' => $feedback->count(),
            'bookingAmount' => $bookingAmount->get()->pluck('BookingAmount')->sum(),
            'bookingCNAmount' => $bookingCNAmount->get()->pluck('CancelAmount')->sum(),
            'NextBooking' => $NextBooking,
            'bookingCurrent' => $bookingCurrent,
        ]);

    }


}
