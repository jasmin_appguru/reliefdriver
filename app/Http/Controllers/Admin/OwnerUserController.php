<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Owner;
use App\Models\User;
use App\Models\Driver;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AllAgentsExport;
use App\Exports\OwnerExport;
use Yajra\Datatables\Datatables;

class OwnerUserController extends Controller
{
    public function getalldata()
    {
        if(isset($_REQUEST['order'])){
            $users = User::where('user_type',2)->get();
        }
        else{
            $users = User::where('user_type',2)->orderBy('id', 'DESC')->get();
        }

        return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('name', function($users){
                    $fullName = $users->first_name.' '.$users->last_name;
                    $profile = '<a class="avatar" href="javascript:void(0)"> <img alt="" class="img-fluid" src="'.$users->image.'"></a>';
                    return $fullName;
                })
                ->editColumn('join_date', function($users){
                    return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $users->created_at)->format('d - m - y');
                })
                ->editColumn('created', function($users){
                    if($users->Membership)
                    {
                    return date("d - m - y",strtotime($users->Membership->created_at));
                    }
                    else
                    {
                        return "";
                    }
                })
                ->editColumn('start_date_js', function($users){
                    if($users->Membership)
                    {
                    return date("d - m - y",strtotime($users->Membership->start_date));
                    }
                    else
                    {
                        return "";
                    }
                })
                ->editColumn('end_date_js', function($users){
                    if($users->Membership)
                    {
                    return date("d - m - y",strtotime($users->Membership->end_date));
                    }
                    else
                    {
                        return "";
                    }
                })
                ->editColumn('update_date_js', function($users){
                    if($users->Membership)
                    {
                    return date("d - m - y",strtotime($users->Membership->updated_at));
                    }
                    else
                    {
                        return "";
                    }
                })
                ->editColumn('subscription', function($users){
                    if($users->Membership)
                    {
                        $end_date=date('Y-m-d',strtotime($users->Membership->end_date));
                        $is_active='<span class="badge badge-success" > YES </span>';
                        if($end_date < date("Y-m-d"))
                        {
                            $is_active='<span class="badge badge-danger" data="'.$end_date.'"> No </span>';
                        }
                    }
                    else
                    {
                        $is_active='<span class="badge badge-danger">NO</span>';
                    }
                    return $is_active;
                })
                ->editColumn('email', function($users){
                    $email = isset($users->email)?trim($users->email):"N/A";
                    return $email;
                })
                ->editColumn('mobile', function($users){
                    $mobile = isset($users->mobile_number) ? trim($users->mobile_number) : "N/A";
                    return $mobile;
                })
                ->editColumn('agency_name', function($users){
                    $agency_name = isset($users->Owner->business_name) ? trim(\Str::limit($users->Owner->business_name,20)) : "";
                    return $agency_name;
                })
                ->addColumn('status', function($users){
                    $is_active = ($users->is_active == 0) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';
                    return $is_active;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/owner-user/".$user->id);
                    },
                ])
                ->rawColumns(['name','subscription'])
                ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = "Owner List";
        $users = Owner::orderBy('id', 'DESC')->get();
        // dd($users);
        return view('admin.OwnerUser.index', compact('users', 'pageTitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = "User Owner";

        $user = User::where('id',$id)->first();
        if($user){
            return view('admin.OwnerUser.view', compact('user','page'));
        }
        else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exportUser()
    {
      return Excel::download(new OwnerExport(User::all()), 'Relief Driver - Owners - '.date('d-m-Y').'.csv');

    }
}
