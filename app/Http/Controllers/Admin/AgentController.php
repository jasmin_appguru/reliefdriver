<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Agent;
use App\Models\Driver;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AllAgentsExport;
use App\Models\User;
// use App\Models\Country;
// use App\Models\City;
use Redirect;
use Yajra\Datatables\Datatables;
use File;

class AgentController extends Controller
{

    public function index()
    {

        $pageTitle = "Agent List";
        $users = Driver::orderBy('id', 'DESC')->get();
        // dd($users);
        return view('admin.userManagement.index', compact('users', 'pageTitle'));
    }


    public function getalldata()
    {
        if(isset($_REQUEST['order'])){
            $users = Driver::get();
        }
        else{
            $users = Driver::orderBy('id', 'DESC')->get();
        }

        return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('name', function($users){
                    $fullName = $users->first_name.' '.$users->last_name;
                    //echo $fullName;die;
                    $profileUrl = ($users->agent_image) ? url('uploads/agent_photos/'.$users->agent_image) : url('uploads/default-avatar.png');
                    $profile = '<a class="avatar" href="javascript:void(0)">
                        <img alt="" class="img-fluid" src="'.$profileUrl.'"></a>';

                    return $fullName;

                })
                ->editColumn('email', function($users){
                    $email = isset($users->email)?trim($users->email):"N/A";
                    return $email;
                })
                ->editColumn('mobile', function($users){
                    $mobile = isset($users->mobile) ? trim($users->mobile) : "N/A";
                    return $mobile;
                })
                ->editColumn('agency_name', function($users){
                    $agency_name = isset($users->agency_name) ? trim($users->agency_name) : "N/A";
                    return $agency_name;
                })
                ->addColumn('status', function($users){
                    $is_active = ($users->is_active == 0) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';
                    return $is_active;
                })
                ->setRowClass('viewInformation')
                ->setRowAttr([
                    'data-id' => function($user) {
                        return $user->id;
                    },
                    'data-url' => function($user) {
                        return url("admin/agent-management/".$user->id);
                    },
                ])
                ->rawColumns(['name','status'])
                ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $page = "User Details";

        $user = User::where('id',$id)->first();
        // $countries = Country::all();
        // $citys = City::where('country_id',$user->country)->orderBy('id','ASC')->get();
        if($user){
            return view('admin.userManagement.view', compact('user','page'));
        }
        else{
            return view('admin.layouts.includes.modalError');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $user = Agent::find($id);

        $page = "User Details";
        if($user){
            $updateData = [
                "name" => $request->name,
                "last_name" => $request->last_name,
                "dob" => $request->dob,
                "country" => $request->country,
                "city"  => $request->city,
            ];

            if($request->user_profile_pic){
                $imageMainPath = '/uploads/profile_pics/';
                $imageThumbPath = $imageMainPath.'/thumbnail/';
                if (!is_dir(public_path($imageMainPath))) {
                    mkdir(public_path($imageMainPath), 777, true);
                }
                if (!is_dir(public_path($imageThumbPath))) {
                    mkdir(public_path($imageThumbPath), 777, true);
                }

                $profile_image = "user-pro-".time().".png";
                $path = public_path($imageMainPath.$profile_image);

                $image = $request->user_profile_pic;  // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                \File::put($path, base64_decode($image));

                $updateData['profile_pic'] = $profile_image;
            }

            Agent::where("id",$id)->update($updateData);

            return Redirect::to("admin/agent-management")->with("success","Agent details has been updated successfully.");
        }
        else{
            return Redirect::to("admin/agent-management")->with("error","Something went wrong. Please try again!!");
        }
    }


    public function destroy($id)
    {
        $userDB = Agent::find($id);

        if($userDB)
        {
            $userDB->delete();

            return redirect()->back()->with("success","Agent has been deleted successfully.");
        }
        else
        {
            return redirect()->back()->with("error","Opps!! Something went wrong. Please try again.");
        }
    }



    public function exportUser()
    {
      return Excel::download(new AllAgentsExport, 'AllAgents-'.date('d-m-Y').'.csv');

    }
    public function inactiveUser($id)
    {
        $updateData = [ 'is_active' => 1, ];
        Agent::where("id",$id)->update($updateData);
        return Redirect::back()->with("success","Agent has been in-activated successfully.");

    }
    public function activeUser($id)
    {
        $updateData = [ 'is_active' => 0, ];
        Agent::where("id",$id)->update($updateData);
        return Redirect::back()->with("success","Agent activated successfully.");

    }


}
