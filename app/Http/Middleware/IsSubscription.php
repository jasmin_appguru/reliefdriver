<?php

namespace App\Http\Middleware;

use App\Models\MemberShip;
use Closure;
use Illuminate\Http\Request;

class IsSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check())
        {
            $user_old=MemberShip::where('user_id',\Auth::user()->id)->where('end_date', '>', date('Y-m-d h:i:s') )->first();
            if(empty($user_old))
            {
                $response = [
                    'status' => 777,
                    'message' => "Please subscribe to continue using the app. You might not able to perform some actions to your job until you made the subscription.",
                    'serverDate' => date('d/m/Y h:i:s'),
                ];
                return response()->json($response, 200);
            }
        }
        return $next($request);
    }
}
