<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;

class IsOwner
{
    public $response;
    public function __construct()
    {
        $this->response=new BaseController();
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(\Auth::user()->user_type==2)
        {
            return $next($request);
        }
        else
        {
            return $this->response->handleError("This Request is only for Owner user.");
        }
    }
}
