<?php

namespace App\Jobs;

use App\Models\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Database\Eloquent\Builder;

class NotificationNextDayTrigger implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info("Call Australia/Birshbane  At 03:00 HANDLE");
        \Log::info("Reminder For Tomorrows");
        $booking_limit=Booking::Active()->where('is_accept',1)
        ->with(['bookingDates','Owner','Driver'])
        ->whereHas('bookingDates', function (Builder $query) {
            $tomorrow = date("Y-m-d", strtotime('+1 day'));
            $query->where('date', $tomorrow);
           })->get();

           \Log::info("Booking Found ".count($booking_limit));

           foreach($booking_limit as $booking_detail)
           {
            //Reminder! You have a booking with <owner driver name> tomorrow DD MMM YYYY.
                \Log::info("Notification Send to ".$booking_detail->Owner->User->first_name);
                //SendNotification("Reminder! You have a booking with ".$booking_detail->Owner->User->first_name." tomorrow ".date("d M Y", strtotime('+1 day')).".",$booking_detail->driver->user->id);
           }
           \Log::info("End Reminder For Tomorrows");
    }
}
