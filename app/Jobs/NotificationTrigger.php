<?php

namespace App\Jobs;

use App\Models\Admin;
use App\Models\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Database\Eloquent\Builder;

class NotificationTrigger implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //Australia/Sydney

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info("Call Australia/Sydney At 17:00 HANDLE");
        \Log::info("Reminder For Tomorrows");
        $booking_limit=Booking::Active()->where('is_accept',1)
        ->with(['bookingDates','Owner','Driver'])
        ->whereHas('bookingDates', function (Builder $query) {
            $tomorrow = date("Y-m-d", strtotime('+1 day'));
            $query->where('date', $tomorrow);
           })->get();

           \Log::info("Booking Found ".count($booking_limit));

           foreach($booking_limit as $booking_detail)
           {
            //Reminder! You have a booking with <owner driver name> tomorrow DD MMM YYYY.
                \Log::info("Notification Send to ".$booking_detail->Owner->User->first_name);
                SendNotification("Reminder! You have a booking with ".$booking_detail->Owner->User->first_name." tomorrow ".date("d M Y", strtotime('+1 day')).".",$booking_detail->driver->user->id);
           }
           \Log::info("End Reminder For Tomorrows");


        //    \Log::info("Reminder For 7 Days");
        //    $booking_limit=Booking::Active()->where('is_accept',1)
        //    ->with(['bookingDates' => function ($query) {
        //        $query->first();
        //    },'Owner','Driver'])
        //    ->whereHas('bookingDates', function (Builder $query) {
        //        $tomorrow = date("Y-m-d", strtotime('+7 day'));
        //        $query->where('date', $tomorrow);
        //       })->get();

        //       \Log::info("Booking Found ".count($booking_limit));

        //       foreach($booking_limit as $booking_detail)
        //       {
        //        //Reminder! You have a booking with <owner driver name> tomorrow DD MMM YYYY.
        //            \Log::info("Notification Send to ".$booking_detail->Owner->User->first_name);
        //            SendNotification("Reminder! You have a booking with ".$booking_detail->Owner->User->first_name." at ".date("d M Y", strtotime('+7 day')).".",$booking_detail->driver->user->id);
        //       }
        //       \Log::info("End Reminder For 7 Days");


        //    \Log::info("Reminder For Today");
        //    $booking_limit=Booking::Active()->where('is_accept',1)
        // ->with(['bookingDates' => function ($query) {
        //     $query->first();
        // },'Owner','Driver'])
        // ->whereHas('bookingDates', function (Builder $query) {
        //     $tomorrow = date("Y-m-d");
        //     $query->where('date', $tomorrow);
        //    })->get();

        //    \Log::info("Booking Found ".count($booking_limit));

        //    foreach($booking_limit as $booking_detail)
        //    {
        //         SendNotification("Reminder! You have a booking with ".$booking_detail->Owner->User->first_name." in 3 hours.",$booking_detail->driver->user->id);
        //    }
        //    \Log::info("End Reminder For Today");
    }
}
