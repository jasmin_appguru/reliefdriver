<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Twilio\Rest\Notify;

class Owner extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'business_name',
        'truck_location',
        'truck_number',
        'latitude',
        'longitude',
    ];

    protected static function boot()
    {
        parent::boot();
        $closure = function ($model) {
            if($model->isDirty('truck_number')){
                $booking_past=Booking::Past()->where('request_user_id',$model->id)->pluck('id')->toArray();

                $booking=Booking::Active()->with('bookingDates')->where('request_user_id',$model->id)->whereNotIn('id',$booking_past)->update([
                    'truck_no' => $model->truck_number
                ]);

                $bookingRq=Booking::Active()->with('bookingDates')->where('request_user_id',$model->id)->where('is_accept',0)->whereNotIn('id',$booking_past)->pluck('driver_id')->toArray();
                foreach(array_unique($bookingRq) as $select_driver)
                {
                    SendNotification("The truck number has been changed by ".$model->user->full_name." for your booking request. Please check your booking request details. ",Driver::find($select_driver)->User->id);
                }

                $booking=Booking::Active()->with('bookingDates')->where('request_user_id',$model->id)->where('is_accept',1)->whereNotIn('id',$booking_past)->pluck('driver_id')->toArray();
                foreach(array_unique($booking) as $select_driver)
                {
                    SendNotification("The truck number has been changed by ".$model->user->full_name." for your booking. Please check your current booking details. ",Driver::find($select_driver)->User->id);
                }
              }

            if($model->isDirty('truck_location')){
                $booking_past=Booking::Past()->where('request_user_id',$model->id)->pluck('id')->toArray();

                $booking=Booking::Active()->with('bookingDates')->where('request_user_id',$model->id)->whereNotIn('id',$booking_past)->update([
                    'truckLocation' => $model->truck_location,
                    'latitude' => $model->latitude,
                    'longitude' => $model->longitude,
                ]);

                $bookingRq=Booking::Active()->with('bookingDates')->where('request_user_id',$model->id)->where('is_accept',0)->whereNotIn('id',$booking_past)->pluck('driver_id')->toArray();
                foreach(array_unique($bookingRq) as $select_driver)
                {
                    // The location of the truck has been changed by “OWNER FULL NAME” for your booking request. Please check your booking request details.
                    SendNotification("The location of the truck has been changed by ".$model->user->full_name." for your booking request. Please check your booking request details.",Driver::find($select_driver)->User->id);
                }

                $booking=Booking::Active()->with('bookingDates')->where('request_user_id',$model->id)->where('is_accept',1)->whereNotIn('id',$booking_past)->pluck('driver_id')->toArray();
                foreach(array_unique($booking) as $select_driver)
                {
                    // The location of the truck has been changed by “OWNER FULL NAME” for your booking. Please check your current booking details.
                    SendNotification("The location of the truck has been changed by ".$model->user->full_name." for your booking. Please check your current booking details.",Driver::find($select_driver)->User->id);
                }
              }
            // Booking();
            // User::where('age', '<', 18)->update(['under_18' => 1]);

        };
        static::updated($closure);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
