<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverUnavailableDate extends Model
{
    use HasFactory;

    protected $fillable=['driver_id','date'];

    public function Driver() {
        return $this->belongsToMany(Driver::class,'driver_id');
    }
}
