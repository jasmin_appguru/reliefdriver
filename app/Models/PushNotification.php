<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model
{
    use HasFactory;

    public function getTypeAttribute($value)
    {
        if($value!='All')
        {
            return $value."s";
        }
        else
        {
            return $value;
        }
    }
}
