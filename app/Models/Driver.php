<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\DriverUnavailableDate;

class Driver extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'availability',
        'is_inducted_in',
        'inducted_company',
        'industry_experience',
        'day_rate_a',
        'day_rate_b',
        'night_rate_a',
        'night_rate_b',
        'license_class',
        'hasAvailableDays',
    ];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function UnAvailableDates()
    {
        return $this->hasMany(DriverUnavailableDate::class)->orderBy('date');
    }
}
