<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use App\Notifications\WelcomeMail;
use Laravel\Passport\HasApiTokens;
use Notification;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    public $varta;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'device_type',
        'user_type',
        'mobile_number',
        'image',
        'device_token',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['isSubscribed','ExpireAt'];

    public function getIsSubscribedAttribute($q)
    {
        //$select=$this->Membership()->where('end_date',">",date('Y-m-d'))->count();
        $select=$this->Membership()->count();
        if($select>0)
        {
            return 1;
        }
        return 0;
    }

    public function getExpireAtAttribute($q)
    {

        $select=$this->Membership()->first();
        if(isset($select))
        {
            return date('d/m/Y h:i:s',strtotime($select->end_date));
        }
        return '';
    }

    public function getImageAttribute($value)
    {
        $this->varta=$value;
        if(empty($value))
        {
            $value="default.jpg";
        }
        return asset("storage/avatars/".$value);
    }

    public function getFullNameAttribute() {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    public function getThumbnailAttribute()
    {
        return asset("storage/avatars/thumb/".$this->varta);
    }



    public function Driver()
    {
        return $this->hasOne(Driver::class);
    }

    public function Owner()
    {
        return $this->hasOne(Owner::class);
    }

    public function Membership()
    {
        return $this->hasOne(MemberShip::class);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token,$this->email,$this));
    }

    public function WelcomeMail()
    {
            $details = [
                'greeting' => 'Hello '.$this->first_name.", ",
                'body' => 'This is my first notification from Nicesnippests.com',
                'thanks' => 'Thank you for using Nicesnippests.com tuto!',
                'actionURL' => url('/'),
            ];
            //$this->notify(new WelcomeMail($details));
            Notification::send($this, new WelcomeMail($details));
    }
}
