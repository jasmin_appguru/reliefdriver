<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Booking extends Model
{
    use HasFactory;
    protected $appends = ['BookingAmount','S_date'];
    protected $casts = [
        'bookingAmount' => 0,
        'cancal' => 0
    ];

    public function getBookingAmountAttribute($q)
    {
        //dd($this->calculateBooking($q,0));
        return $this->calculateBooking($q,0);
    }
    public function getSDateAttribute($q)
    {
        return $this->bookingDates()->orderBy('date','asc')->first()->date ?? '';
    }
    public function getCancelAmountAttribute($q)
    {
        return $this->calculateBooking($q,1);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $driver=Driver::find($model->driver_id);
            if($model->shift=="Day")
            {
                $model->amount = $driver->day_rate_a;
            }
            else
            {
                $model->amount = $driver->night_rate_a;
            }
        });
    }

    public function calculateBooking($query,$val)
    {
        return $this->bookingDates()->where('is_canceled',$val)->count() * $this->amount;
    }

    public function scopePast($query)
    {
        return $query->whereHas('bookingDates',function($q){
            $q->where('date',"<",date('Y-m-d'));
        });
    }

    public function scopeActive($query)
    {
        return $query->whereHas('bookingDates',function($q){
            $q->where('date',">=",date('Y-m-d'));
            $q->orderBy('date', 'asc');
        });
    }

    public function getShiftAttribute($value)
    {
        return ucfirst($value);
    }

    public function bookingDates()
    {
        return $this->hasMany(BookingDate::class)->orderBy('id', 'asc');
    }

    public function Owner()
    {
        return $this->belongsTo(Owner::class,'request_user_id');
    }

    public function Driver()
    {
        return $this->belongsTo(Driver::class,'driver_id');
    }

    public function Details()
    {
        return $this->hasOne(RequestCancel::class);
    }

}
