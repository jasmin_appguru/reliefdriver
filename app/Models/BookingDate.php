<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingDate extends Model
{
    use HasFactory;

    protected $fillable = ['is_accept'];

    public function scopeActive($query)
    {
        return $query->where('date',">",date('Y-m-d'));
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class,'driver_id');
    }

    public function Booking()
    {
        return $this->belongsTo(Booking::class);
    }
}
