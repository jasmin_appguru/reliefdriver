<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestCancel extends Model
{
    use HasFactory;

    protected $fillable=['driver_id','reason'];

    public function Booking()
    {
        return $this->belongsTo(Booking::class);
    }

}
