<?php

namespace App\Console\Commands;

use App\Models\Admin;
use App\Models\Faq;
use Illuminate\Console\Command;

class FlushDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:flush';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command will Remove all Data From Database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->confirm('Are you sure you want to remove all db Data ?', true)) {
            $this->comment('Logs have been cleared!');
            $password = $this->secret('Please Enter Key Which is Provided By Appgurus ?');
            if($password=="Appgurus$2022")
            {
                \Log::info("\n DB Clean By Appgurus Administrator @ : ".date('Y-d-m h:i:s'));
                $this->info("DB Clean precess Start");
                \Artisan::call('migrate:fresh');
                $this->warn("DB Clean Successfully.");
                \Artisan::call('passport:install');
                $this->info("Added Passport Access token Added ");
                $faq=Faq::create([
                    "question" => "Is there a booking fee?",
                    "answer" => "There are no associated fees to book a Relief Driver. You simply pay your monthly subscription of $6.99 in order to use the app."
                ],
                [
                    "question" => "Can I pay my Relief Driver through the app?",
                    "answer" => "For now, we are only facilitating bookings. You are required to pay your Relief Driver by their preferred payment method."
                ],
                [
                    "question" => "What happens if my Relief Driver doesn\'t show up?",
                    "answer" => "A Relief Driver who doesn’t show or notify you of changes of circumstances and availability to do the booked job is classified unreliable. We recommend that you book a different Relief Driver in the future."
                ],
                [
                    "question" => "Can I sign up as an Owner and Relief Driver with one account?",
                    "answer" => "You need to have a separate account as an Owner Driver and Relief Driver. If you sign up as an Owner Driver, and then want to sign up as a Relief Driver, you will need to use a different email address."
                ],
                [
                    "question" => "What do I do if there are any issues with the app?",
                    "answer" => "If you encounter any issue, please report it to us via the Feedback section. Our aim is to keep the app operating at an optimal level however due to unforeseen changes in operating systems and devices, unexpected hurdles may occur. It is highly recommended that you ensure your mobile phone has the latest operating system update installed."
                    ]
                );
                $this->warn("FAQ Added SuccessFully.");
                $this->info("Is there a booking fee?\nCan I pay my Relief Driver through the app?\nWhat happens if my Relief Driver doesn\'t show up?\nCan I sign up as an Owner and Relief Driver with one account?\nWhat do I do if there are any issues with the app?");
                $admin=Admin::create(["name"=> "Adam","mobile"=>"1231231231","email"=>"admin@reliefdriver.co","password"=>bcrypt("Admin@321!"),"profile_pics"=>""]);
                $this->warn("Admin Account Is Created Successfully.");
                $this->info("Email: \nadmin@reliefdriver.co\nPassword: \nAdmin@321!");
                $this->info("http://reliefdriver.co/admin/");
            }
            else
            {
                $this->error("Invalid Reset DB Password.");
            }

        }
        return 0;
    }
}
