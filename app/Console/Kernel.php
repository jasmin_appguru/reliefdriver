<?php

namespace App\Console;

use App\Jobs\NotificationTrigger;
use App\Jobs\NotificationNextDayTrigger;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        Commands\DailyQuote::class,
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //\log::indo("cron working");
        $schedule->job(new NotificationTrigger)->dailyAt('18:00')->timezone('Australia/Brisbane');
        $schedule->job(new NotificationNextDayTrigger)->dailyAt('18:00')->timezone('Australia/Brisbane');
        $schedule->command('queue:work', [
            '--max-time' => 300
            ])->withoutOverlapping();
            //\Log::info("call Con");
        //$schedule->command('queue:work')->everyFiveMinutes()->withoutOverlapping();
        }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
