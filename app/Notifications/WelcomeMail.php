<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WelcomeMail extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user=$user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // ->greeting('Welcome to Tradie Tracer')
        return (new MailMessage)
                    ->subject("Welcome To ".config('app.name'))
                    ->greeting($this->user['greeting'])
                    ->line("We’re super excited to have you join our platform.")
                    ->line("We created Relief Driver to eliminate the booking hassles most Relief and Owner drivers experience. Our aim is to bridge the gap and provide a fantastic booking solution as a foundation for what is yet to come. ")
                    ->line("Please share your feedback with us directly via the app after your first couple of bookings.");




                    // ->line("Out of courtesy and for everyone's safety, we have developed an app with simple contact tracing that will give tradies and clients peace of mind.");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
