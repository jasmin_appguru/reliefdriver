<?php

use App\Http\Controllers\Admin\BookingController;
use App\Http\Controllers\Admin\DriverUserController;
use App\Http\Controllers\Admin\FeedbackController;
use App\Http\Controllers\Admin\OwnerUserController;
use App\Http\Controllers\Admin\NotificationManagerController;
use App\Http\Controllers\Admin\PushNotificationController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TwilioSMSController;
use App\Jobs\NotificationTrigger;
use App\Models\Admin\PushNotification;
use App\Models\OtpManager;
use App\Models\Owner;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('commingsoon');
})->name('frontend.home');

Route::get('/privacy-policy', function () {
    return view('policy');
});

Route::get('/success', function () {
    return view('success');
});

Route::get('/terms-and-conditions', function () {
    return view('tc');
});

// Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/send',[TwilioSMSController::class,'index']);

Route::get('/crontest',function(){
    \Log::info("Add NotificationTrigger");
    NotificationTrigger::dispatch();
    // NotificationTrigger()
    //NotificationTrigger
    //dd(\Config::get('relief_driver.MAIL.HEADER_COLOR'));
    // return view('email.reset');
});

Route::get('CountCounter',[BookingController::class,'CountCounter']);

Route::group(['namespace' => 'App\Http\Controllers'], function()
    {
		//Auth::routes();
		/* Start Custom Auth URL's */
			// Authentication Routes..
			Route::post('logout', 'Auth\LoginController@logout')->name('logout');
			// Registration Routes...
			Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
			Route::post('register', 'Auth\RegisterController@register');
			// Password Reset Routes...
			Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
			Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
			Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
			Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
			// Email Verification Routes...
			Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
			Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
			Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
			// Admin Urls
			Route::get('admin/login', 'Adminauth\LoginController@showLoginForm')->name('adminLoginFrom');
			Route::post('admin/login', 'Adminauth\LoginController@adminLogin')->name('adminlogin');
			Route::post('admin/logout', 'Adminauth\LoginController@logout')->name('adminlogout');
			Route::get('/admin/password/reset','Adminauth\ForgotPasswordController@showForgotPasswordForm')->name('forgotpasswordform');
			Route::post('/admin/password/email','Adminauth\ForgotPasswordController@sendResetLinkEmail')->name('adminforgotpassword');
			Route::get('/admin/password/reset/{token}','Adminauth\ResetPasswordController@showResetPasswordForm')->name('showResetPasswordForm');
			Route::post('/admin/password/reset','Adminauth\ResetPasswordController@reset')->name('adminResetPassword');
			/* END Custom Auth URL's */
    });

	Route::group(['prefix' => 'admin', 'namespace' => 'App\Http\Controllers\Admin','middleware' => ['auth:admin']], function()
	{
		/*DASHBOARD CONTROLLER*/
		Route::get('/','DashboardController@index');
        Route::get('/getOtp/{mobile}',function($moile){
            $otp=OtpManager::where('mobile_number',$moile)->first();
            if($otp)
            {
                return $otp->otp;
            }
            else
            {
                return "invalid number";
            }
        });
		Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
        Route::post('dashboard', 'DashboardController@filter')->name('filter');
		Route::resource('user-profile', 'ProfileController');
        Route::post('/changePassword/{id}','ProfileController@changePassword')->name('changePassword');
        /*NOTIFICATION MESSAGE CONTROLLER*/
        Route::get('notification-messages/getAllData', 'NotificationMessageController@getAllData');
        Route::resource('notification-messages', 'NotificationMessageController');
        /*REGISTERED USER CONTROLLER*/
        Route::resource('owner-user', 'OwnerUserController');
        Route::get('exportOwner', 'OwnerUserController@exportUser');
        Route::resource('driver-user', 'DriverUserController');
        Route::get('exportDriver', 'DriverUserController@exportUser');
        Route::resource('Booking', 'BookingController');
        Route::resource('Feedback', 'FeedbackController');
        Route::resource('Notification', 'PushNotificationController');

        Route::get('agent-management/getAllData', 'AgentController@getalldata');
        //('agent-management/getAllData', 'AgentController@getalldata');
        Route::get('exportAgentData', 'AgentController@exportUser');
        Route::get('activeUserAgent/{id}', 'AgentController@activeUser');
        Route::get('inactiveUserAgent/{id}', 'AgentController@inactiveUser');
        Route::resource('agent-management', 'AgentController');
		Route::get('buyer-management/getAllData', 'BuyerController@getalldata');
        Route::get('exportBuyerData', 'BuyerController@exportUser');
        Route::get('activeUserBuyer/{id}', 'BuyerController@activeUser');
        Route::get('inactiveUserBuyer/{id}', 'BuyerController@inactiveUser');
        Route::resource('buyer-management', 'BuyerController');
        Route::get('user-feedbacks/getalldata', 'FeedbackController@getalldata');
        Route::resource('user-feedbacks', 'FeedbackController');

		// Route::resource('affiliation-management', 'Aff');
        // Route::get('Aff/getAllData', 'Aff@getAllData');
        // Route::post('affiliation-management/store', 'Aff@store')->name('createAffiliate');
        // Route::post('affiliation-management/update/{id}', 'Aff@edit')->name('affiliate-management.update');
        // Route::post('affiliation-management/destroy/{id}', 'Aff@destroy')->name('affiliate-management.destroy');

	});

    Route::group(['prefix' => 'admin-api'], function()
    {
        Route::get('owner-user/getAllData', [OwnerUserController::class,'getalldata']);
        Route::get('driver-user/getAllData', [DriverUserController::class,'getalldata']);
        Route::get('booking-list/getAllData', [BookingController::class,'getalldata']);
        Route::get('feedback-list/getAllData', [FeedbackController::class,'getalldata']);
        Route::get('Notification/getAllData', [PushNotificationController::class,'getalldata'])->name('Ajax.Notification');
    });
