<?php

use App\Http\Controllers\Api\NotificationController;
use App\Http\Controllers\Api\BookingController;
use App\Http\Controllers\Api\DriverController;
use App\Http\Controllers\Api\FeedbackController;
use App\Http\Controllers\Api\OwnerController;
use App\Http\Controllers\Api\SubscriptionController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\FaqController;
use App\Http\Middleware\IsDriver;
use App\Http\Middleware\IsOwner;
use App\Http\Middleware\IsSubscription;
use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('signUp',[UserController::class,'register']);
Route::post('login',[UserController::class,'login']);
Route::post('ForgotPassword',[UserController::class,'ForgotPassword']);
Route::post('getVerificationCode',[UserController::class,'getVerificationCode']);
Route::post('getDriverProfile',[DriverController::class,'getDriverProfile']);
Route::post('getOwnerProfile',[OwnerController::class,'getOwnerProfile']);

Route::middleware('auth:api')->group( function () {
    /**
     * List of Basic Authentication
     * Our Base table is user Laravel authentication
     */

     //Subscription
     Route::post('checkReceipt',[SubscriptionController::class,'GetSubscription']);
     Route::post('setMemberShip',[SubscriptionController::class,'setMemberShip']);
    //  GetSubscription
    Route::any('welcome',[UserController::class,'WelcomEmail']);
    Route::post('changePassword',[UserController::class,'changePassword']);
    Route::post('Logout',[UserController::class,'Logout']);
    ////////////// removed
    // Route::post('completeProfile',[UserController::class,'completeProfile']);
    // Route::post('editProfile',[UserController::class,'editProfile']);
    /**
     * List of Relief Driver Api list
     * When user type = 1
     */
    Route::post('completeProfileDriver',[DriverController::class,'completeProfile']);
    // Validated Request is Driver or not

    Route::middleware([IsDriver::class])->group(function () {
        Route::post('editProfileDriver',[DriverController::class,'editProfileDriver']);
        Route::middleware([IsSubscription::class])->group(function () {
            Route::post('RequestOperation',[BookingController::class,'RequestOperation']);
        });
        Route::post('againAcceptBookingRequest',[BookingController::class,'AgainRequestOperation']);
        Route::post('currentBookingDriver',[BookingController::class,'index']);
        Route::post('getBookingRequestList',[BookingController::class,'getBookingRequestList']);
    });

    /**
     * List of Relief Owner Api list
     * When user type = 2
     */
    Route::post('completeProfileOwner',[OwnerController::class,'completeProfile']);
    // Validated Request is Owner or not

    Route::middleware([IsOwner::class])->group(function () {
        Route::middleware([IsSubscription::class])->group(function () {
            Route::post('filterReliefDriver',[BookingController::class,'filterReliefDriver']);
            Route::post('searchReliefDriver',[BookingController::class,'searchReliefDriver']);
            Route::post('getReliefDriverList',[OwnerController::class,'getReliefDriverList']);
            Route::post('sendBookingRequest',[BookingController::class,'sendBookingRequest']);
            Route::post('againSendBookingRequest',[BookingController::class,'againSendBookingRequest']);
            Route::post('cancelBookings',[BookingController::class,'cancelBookings']);
        });
        Route::post('editProfileOwner',[OwnerController::class,'editProfileOwner']);
        Route::post('currentBookingOwner',[BookingController::class,'index']);
        Route::post('getSendBookingRequest',[BookingController::class,'getSentBookingRequest']);
    });

    /**
     * @author Jasmin Shukla
     * Booking Module Route Start
     */




    /**
     * @author Jasmin Shukla
     * Application Request accept/decline
     * 0 = decline
     * 1 = Accept
     */


    /**
     * @author Jasmin Shukla
     * Application Request accept/decline
     * 0 = decline
     * 1 = Accept
     */


    Route::post('SendFeedback',[FeedbackController::class,'SendFeedback']);

    Route::post('faq',[FaqController::class,'index']);

    Route::post('getNotificationList',[NotificationController::class,'GetNotificationList']);
    Route::post('notificationRead',[NotificationController::class,'NotificationRead']);

    Route::post('setMemberShip',[SubscriptionController::class,'setMemberShip']);

    Route::post('LoginUserDetail',[UserController::class,'loginUser']);

    Route::post('deleteAccount',[UserController::class,'deleteUser']);

});
Route::get('/',[UserController::class,'unauthorize']);
